GCC=gcc
FLAGS= -ggdb -Wall
CC=$(GCC) $(FLAGS)

LD_DIR=$(HOME)

DIR_LIST=$(LD_DIR)/include 
INC_LIST=-I$(DIR_LIST)

DIR_SIGNAL=./modules/signal
INC_SIGNAL=-I$(DIR_SIGNAL)
OBJ_SIGNAL=$(DIR_SIGNAL)/obj/signal.o

DIR_BOARD=./modules/board
INC_BOARD=-I$(DIR_BOARD)
OBJ_BOARD=$(DIR_BOARD)/obj/board.o

DIR_DEVICE=./modules/device
INC_DEVICE=-I$(DIR_DEVICE)
OBJ_DEVICE=$(DIR_DEVICE)/obj/device.o

DIR_CLOCK=./modules/clock
INC_CLOCK=-I$(DIR_CLOCK)
OBJ_CLOCK=$(DIR_CLOCK)/obj/clock.o

LD_LIST=$(INC_LIST) -L$(LD_DIR)/lib -llist

BINDIR=./bin

OBJDIR=./obj
deps=$(OBJ_SIGNAL) $(OBJ_BOARD) $(OBJ_DEVICE) $(OBJ_CLOCK)
objects=$(deps) $(OBJDIR)/main.o

.PHONY: all libs test clean install doc signal board device clock

all: $(BINDIR)/gods libs

libs: signal board device clock

signal:
	$(MAKE) libs --directory=modules/$@

modules/signal/obj/signal.o: signal

board: signal
	$(MAKE) libs --directory=modules/$@

modules/board/obj/board.o: board

device: signal board
	$(MAKE) libs --directory=modules/$@

modules/device/obj/device.o: device

clock: device
	$(MAKE) libs --directory=modules/$@

modules/clock/obj/clock.o: clock

$(OBJDIR)/main.o: main.c application.h
	$(CC) -c -o $@ $< $(INC_LIST)

################################################################################
#
#	Application
#

$(BINDIR)/gods: $(objects)
	$(CC) -o $@ $^ $(LD_LIST)

################################################################################
#
#	Test
#

test: $(deps)
	$(MAKE) --directory=modules/signal/$@
	$(MAKE) --directory=modules/board/$@
	$(MAKE) --directory=modules/device/$@
	$(MAKE) --directory=modules/clock/$@
	$(MAKE) --directory=$@

################################################################################
#
#	Clean
#

clean:
	rm -f ./*.gch $(BINDIR)/* ./*.o $(OBJDIR)/*.o
	$(MAKE) clean --directory=modules/signal
	$(MAKE) clean --directory=modules/board
	$(MAKE) clean --directory=modules/device
	$(MAKE) clean --directory=modules/clock
	$(MAKE) clean --directory=test

################################################################################
#
#	Install
#

install: libs
	$(MAKE) install --directory=modules/signal
	$(MAKE) install --directory=modules/board
	$(MAKE) install --directory=modules/device
	$(MAKE) install --directory=modules/clock

################################################################################
#
#	Doxygen
#

doc: libs
	doxygen Doxyfile
