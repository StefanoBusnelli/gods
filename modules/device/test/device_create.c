#include <stdlib.h>
#include <stdio.h>

#include "signal.h"
#include "device.h"

#define SIGN_NUM = 2
#define SIGN_GND = 0
#define SIGN_VCC = 1

int main( int argc, char **argv ) {
  t_device*     d = NULL;
  char    *sign[2] = {"Gnd","Vcc"};

  printf( "\n" );

  printf( "\n---\tCreo device CLOCK" );

  device_create( &d, "CLOCK", sign, 2, NULL, NULL, NULL );

  printf( "\nDevice (t_device):                       %16p %16ld bytes", d, sizeof( d ) );
  if ( d != NULL ) {
  printf( " %16ld bytes", sizeof( *d ) );
  }

  printf( "\nOK.\n" );

  return 0;
}
