/**
 * @file    device.c
 * @author  Stefano Busnelli    busnelli.stefano@gmail.com
 * @version 1.3.0
 * @brief   Device
 * @details Strutture dati del device generico con cui implementare i device e le funzioni
 */

#include "device.h"

/**
 *  @brief      Inizializzazione del device
 *  @details    Esegue la call back di inizializzazione impostata da device_create
 *  @param      *d  Puntatore al device
 */
void device_init( t_device *d ) {
  d->internal.dev_state = ds_initializing;
  if ( d->internal.cb_init != NULL )
    d->internal.cb_init( d );
  d->internal.dev_state = ds_initialized;
}

/**
 *  @brief      Esecuzione del main loop
 *  @details    Controlla gli stati di esecuzione e la condizioone d'uscita del main loop, Esegue la call back di run impostata da device_create
 *  @param      d   Puntatore al device
 */
void device_run( t_device* d ) {
  //    Eseguo main loop
  //    Se la board è in run_stare b_device vale rm_run, altrimenti verrà sovrascritto.
  d->internal.dev_state      = ds_starting;
  while ( d->internal.main_loop == true ) {
    d->internal.steps++;

    //  Test run state device
    //      Vado in pause prima di eseguire il codice del device
    if ( d->internal.run_mode == rm_pause ) {
      d->internal.dev_state        = ds_pausing;
      //    Devo testare anche il run_state della board per permettere l'uscita dallo stato di pause
      while( d->internal.run_mode == rm_pause ) {
        d->internal.dev_state      = ds_paused;
      }
    }
    //      Esecuzione per step: la prima volta imposto lo step di terminazione, poi entro in pausa
    if ( d->internal.run_mode == rm_step ) {
      if ( d->internal.step_until < d->internal.steps ) {
        d->internal.step_until = d->internal.steps + 1;
      } else {
        d->internal.run_mode = rm_pause;
      }
    }
    //      Eseguo il codice del device, sia un stato run che in stato step
    if ( d->internal.run_mode == rm_run || d->internal.run_mode == rm_step ) {
      d->internal.dev_state        = ds_running;
      if ( d->internal.cb_run != NULL )
        d->internal.cb_run( d );
    }
    //      Se devo uscire dal main loop non eseguo il codice del device
    if ( d->internal.run_mode == rm_stop ) {
      d->internal.dev_state        = ds_stopping;
      d->internal.main_loop   &= false;
    }
  }
  // Uscita dal main loop
  d->internal.run_mode      = rm_unset;
  d->internal.dev_state = ds_finish;
}

/**
 *  @brief      Fine dell' esecuzione del device
 *  @details    Esegue la call back di end impostata da device_create
 *  @param      d   Puntatore al device
 */
void device_end( t_device* d ) {
  d->internal.dev_state = ds_ending;
  if ( d->internal.cb_end != NULL )
    d->internal.cb_end( d );
  d->internal.dev_state = ds_end;
}

/**
 *  @brief      Creazione del device
 *  @details    Crea un device ed inizializza i valori
 *  @param      **d         Indirizzo del puntatore a t_device
 *  @param      *name       Nome del device
 *  @param      **sig_names Array di stringhe con i nomi dei device oppure NULL
 *  @param      f_ini       Puntatore alla call back di init
 *  @param      f_run       Puntatore alla call back di run
 *  @param      f_end       Puntatore alla call back di end
 *  @return     *d          Puntatore a t_device
 *  @code
 *  t_device *d;
 *  device_create( &d, "dev0", NULL, NULL, NULL, NULL );
 *  @endcode
 */
void device_create( t_device **d, char *name, char **sig_names, unsigned int sig_num, f_device f_ini, f_device f_run, f_device f_end ) {
  t_device *dev = NULL;

  dev = ( t_device* ) calloc( 1, sizeof( t_device ) );
  if ( dev != NULL ) {
    dev->name                     = name;
    dev->data                     = NULL;
    dev->internal.run_mode        = rm_unset;
    dev->init                     = device_init;
    dev->run                      = device_run;
    dev->end                      = device_end;
    dev->internal.sig_desc.sig_names       = sig_names;
    dev->internal.sig_desc.sig_num         = sig_num;
    dev->internal.dev_state       = ds_uninit;
    dev->internal.steps           = 0;
    dev->internal.step_until      = 0;
    dev->internal.main_loop       = true;
    dev->internal.cb_init         = f_ini;
    dev->internal.cb_run          = f_run;
    dev->internal.cb_end          = f_end;
    dev->signals                  = NULL;
    dev->params                   = NULL;
    dev->methods                  = NULL;
    
    //  Spazio per i puntatori ai segnali richiesti dal device
    if ( dev->internal.sig_desc.sig_names != NULL && sig_num > 0 ) {
      dev->signals = ( t_signal** ) calloc( sig_num, sizeof( t_signal* ) );
    }
  }

  *d = dev;
}

/**
 *  @brief      Collegamento segnale al device
 *  @details    Collega un t_signal al device nella posizione i_sig dell' array t_signal->signals
 *  @param      *d          Puntatore a t_device
 *  @param      i_sig       Indice numerico del segnale del device
 *  @param      *s          Segnale t_signal
 *  @code
 *  t_device *d = NULL;
 *  t_signal *s = NULL;
 *  Creazione board, device, segnali ecc...
 *  device_link_signal( d, 0, s );
 *  @endcode
 */
void device_link_signal( t_device *d, unsigned int i_sig, t_signal *s ) {
  if ( d != NULL && d->signals != NULL && s != NULL ) {
    d->signals[ i_sig ] = s;
  }
}
