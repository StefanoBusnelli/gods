#ifndef H_DEVICE

#define H_DEVICE

/**
 * @file    device.h
 * @author  Stefano Busnelli    busnelli.stefano@gmail.com
 * @version 1.3.0
 * @brief   Device
 * @details Strutture dati del device generico con cui implementare i device e le funzioni
 */

#include "signal.h"

//  t_device
typedef struct t_device t_device;

//  t_devices è una lista t_list in cui ogni t_devices_node->data punta ad un t_device
typedef struct t_list t_devices;
typedef struct t_node t_devices_node;

/** @brief  Enum con gli stati interni settati dal device
 */
enum e_dev_state { 
  ds_uninit,            ///<    Non inizializzato
  ds_initializing,      ///<    Ingresso in device_init
  ds_initialized,       ///<    Uscita da device_init
  ds_starting,          ///<    Ingresso in device_run
  ds_finish,            ///<    Uscita da device_run
  ds_ending,            ///<    Ingresso in device_end
  ds_end,               ///<    Uscita da device_end
  ds_pausing,           ///<    Rilavato run_state rs_pause 
  ds_running,           ///<    Rilevato run_state rs_run
  ds_stopping,          ///<    Rilevato run_state rs_stop 
  ds_paused,            ///<    Loop pausa
  ds_error              ///<    Errore 
};

/** @brief  Stati con cui contollare l'esecuzione del main loop
 */
enum e_run_mode  { 
  rm_unset,             ///<    Stato non impostato
  rm_step,              ///<    Stato step, esegue un ciclo del main loop in stato run poi mette in pausa 
  rm_run,               ///<    Stato run, esegue la funzione nel main loop
  rm_pause,             ///<    Stato pausa, rimane in attesa all' inizio del main loop
  rm_stop               ///<    Stato stop, esce dal main loop
};

//  Prototipi di funzioni
typedef void ( *f_device ) ( t_device* );
typedef void ( *f_device_par ) ( t_device*, void* );

//  Struttura dati per implementare parametri definiti dinamicamente nel device ( d->params[] )
typedef struct t_device_param t_device_param;

/** @brief  Parametro del device
 */
typedef struct t_device_param {
  char*     name;       ///<    Nome, descrizione del parametro
  void*     value;      ///<    Valore del parametro
} tt_device_param;

//  Struttura dati per implementare metodi definiti dinamicamente nel device ( d->methods[] )
typedef struct t_device_methods t_device_methods;

/** @brief  Metodo del device implementato
 */
typedef struct t_device_methods {
  char*         name;   ///<    Nome, descrizione del metodo
  f_device_par  func;   ///<    Puntatore alla funzione che implementa il metodo
} tt_device_methods;

//  Dati interni al device
typedef struct t_device_internals t_device_internals;

/** @brief  Segnali gestiti dal device
 */
typedef struct t_device_signals t_device_signals;
typedef struct t_device_signals {
  unsigned int            sig_num;              ///<  Numero di segnali
  char                **sig_names;              ///<  Array di stringhe con i nomi dei segnali, da mappare 1-1 in fase di init del device con t_device->signals con device_link_signal
} tt_device_signals;

/** @brief  Dati interni del device
 */
typedef struct t_device_internals {
  t_device_signals      sig_desc;               ///<  Struttura con i nomi dei segnali gestiti dal device
  enum e_run_mode       run_mode;               ///<  Modalità di esecuzione del main loop ( step, run, pause, stop )
  enum e_dev_state      dev_state;              ///<  Stati interni del device ( notinit, initializing, ... )
  bool                  main_loop;              ///<  Condizione di uscita dal main loop
  unsigned long int     steps;                  ///<  Numero di esecuzioni del main loop
  unsigned long int     step_until;             ///<  Numero di step in cui entrare in pausa
  f_device              cb_init;                ///<  Callback da eseguire per inizializzare il device
  f_device              cb_run;                 ///<  Callback da eseguire con pthread per l'esecuzione del device
  f_device              cb_end;                 ///<  Callback da eseguire per cancellare il device
} tt_device_internals;

/** @brief  Struttura dati del device generico
 */
typedef struct t_device {
  char*                 name;                   ///<  Nome device
  void*                 data;                   ///<  Puntatore ad una struct con i dati interni del device
  f_device              init;                   ///<  Metodo per inizializzare il device
  f_device              run;                    ///<  Metodo per l'esecuzione del device
  f_device              end;                    ///<  Metodo per cancellare il device
  t_device_internals    internal;               ///<  Puntatore alla struct dei dati interni del device
  t_signal**            signals;                ///<  Array di puntatori di segnali del dispositivo che puntano a quelli della board
  t_device_param*       params;                 ///<  Array di parametri con cui configurare il device
  t_device_methods*     methods;                ///<  Array di puntatori a struct con cui definire metodi con relativa descrizione da eseguire nel device
} tt_device;

//  -------------------------------------------------------

/**
 *  @brief      Creazione del device
 *  @details    Crea un device ed inizializza i valori
 *  @param      **d         Indirizzo del puntatore a t_device
 *  @param      *name       Nome del device
 *  @param      **sig_names Array di stringhe con i nomi dei device oppure NULL
 *  @param      f_ini       Puntatore alla call back di init
 *  @param      f_run       Puntatore alla call back di run
 *  @param      f_end       Puntatore alla call back di end
 *  @return     *d          Puntatore a t_device
 *  @code
 *  t_device *d;
 *  device_create( &d, "dev0", NULL, NULL, NULL, NULL );
 *  @endcode
 */
void device_create( t_device **d, char *name, char **sig_names, unsigned int sig_num, f_device f_ini, f_device f_run, f_device f_end );

/**
 *  @brief      Collegamento segnale al device
 *  @details    Collega un t_signal al device nella posizione i_sig dell' array t_signal->signals
 *  @param      *d          Puntatore a t_device
 *  @param      i_sig       Indice numerico del segnale del device
 *  @param      *s          Segnale t_signal
 *  @code
 *  t_device *d = NULL;
 *  t_signal *s = NULL;
 *  Creazione board, device, segnali ecc...
 *  device_link_signal( d, 0, s );
 *  @endcode
 */
void device_link_signal( t_device *d, unsigned int i_sig, t_signal *s );

#endif
