#include <stdlib.h>
#include <stdio.h>

#include "board.h"
#include "device.h"

void dump_samples( t_signal_node* node ) {
  t_sample* s = NULL;
  if ( node != NULL ) {
    s = ( t_sample* )node->data;
    if ( s != NULL && s->sample_value != NULL ) {
      printf( "    Sample ID   : %d\tSample Value: ( %16p -> %d )\t", ( int ) s->sample_id, s->sample_value, *( int* )s->sample_value );
    } else {
      printf( "    Sample ID   : %d\tSample Value: %16p\t", ( int ) s->sample_id, s->sample_value );
    }
  }
}

void dump_signals( t_wires_node* node ) {
  t_signal* s = NULL;
  if( node != NULL ) {
    s = ( t_signal* )node->data;
    if ( s != NULL && s->data != NULL ) {
      printf( "\tSignal: \033[33;1m%8s\033[0m\t", (( t_signal_data* ) s->data)->name );
      list_dump( s, NULL, dump_samples, "\n    ", NULL );
    }
  }
}

int main( int argc, char **argv ) {
  t_board         *b    = NULL;
  t_device*        d    = NULL;
  char    *sig_names[]  = { "GND", "VCC", "CLK" };

  printf( "\n" );

  board_create( &b );

  device_create( &d, "CLOCK", sig_names, 3, NULL, NULL, NULL );

  printf( "\n---\tSegnali presenti:" );
  list_dump( b->wires, NULL, dump_signals, "\n  ", NULL );

  printf( "\n---\tCollego device" );
  board_plug_device( b, d );

  printf( "\n---\tSegnali presenti:" );
  list_dump( b->wires, NULL, dump_signals, "\n  ", NULL );

  printf( "\nOK.\n" );

  return 0;
}
