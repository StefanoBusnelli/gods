#include <stdlib.h>
#include <stdio.h>
#include "list.h"
#include "signal.h"
#include "board.h"

void dump_wires( t_wires_node* node ) {
  t_wires* s = NULL;
  if( node != NULL ) {
    s = ( t_wires* )node->data;
    if ( s->data != NULL ) {
      printf( "    Signal: %s", ( char* ) s->data );
    }
  }
}

int main( int argc, char **argv ) {
  t_board *b    = NULL;

  printf( "\nCreo board vuota" );
  board_create( &b );

  printf( "\nBoard:                 %12p %12ld bytes %12ld bytes", b,                 sizeof( b ),                  sizeof( *b )                  );
  printf( "\nBoard->wires:          %12p %12ld bytes %12ld bytes", b->wires,          sizeof( b->wires ),           sizeof( *b->wires )           );

  printf( "\nBoard->wires->nodes:   %12d %12ld bytes", b->wires->nodes ,  sizeof( b->wires->nodes ) );
  printf( "\nBoard->wires->data:    %12p %12ld bytes", b->wires->data  ,  sizeof( b->wires->data  ) );
  printf( "\nBoard->wires->first:   %12p %12ld bytes", b->wires->first ,  sizeof( b->wires->first ) );
  printf( "\nBoard->wires->last:    %12p %12ld bytes", b->wires->last  ,  sizeof( b->wires->last  ) );

  printf( "\nSignals:" );
  list_dump( b->wires, NULL, dump_wires, "\n  ", NULL );

  printf( "\nOk.\n" );

  return 0;
}
