#ifndef H_BOARD

#define H_BOARD

/**
 * @file    board.h
 * @author  Stefano Busnelli    busnelli.stefano@gmail.com
 * @version 1.2.0
 * @brief   Board
 * @details Strutture dati della board su cui montare i devices ed i segnali
 */

#include <stdlib.h>
#include "list.h"
#include "signal.h"
#include "device.h"

/** @brief      t_board
 *  @details    Struttura dati della board, inizializzata con board_create
 */
typedef struct t_board {
  enum e_run_mode   run_mode;       ///<  Stato della board usato per controllare i device ( definito in device.h )
  t_wires*          wires;          ///<  Lista dei segnali nella board
  t_devices*        devices;        ///<  Lista dei devices nella board
} t_board;

//  ----------------------------------------------

/** @brief      Board create
 *  @details    Crea una board ed inizializza i membri della struct
 *  @param      **b Indirizzo del puntatore a t_board
 *  @return     *b  Puntatore a t_board
 */
void board_create( t_board **b );

/** @brief      Plug Device
 *  @details    Inserisce il device nella board, collega i puntatori delle strutture dati ed inizializza il device
 *  @param      *b  Puntatore a t_board
 *  @param      *d  Puntatore a t_device
 */
void board_plug_device( t_board* b, t_device* d );

/** @brief      Set run state
 *  @details    Imposta il run state della board e di tutti i devices montati
 *  @param      *b  Puntatore a t_board
 *  @param      rm  Run state
 */
void board_run_state( t_board *b, enum e_run_mode rm );

#endif
