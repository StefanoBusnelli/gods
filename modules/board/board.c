/**
 * @file    board.c
 * @author  Stefano Busnelli    busnelli.stefano@gmail.com
 * @version 1.2.0
 * @brief   Board
 * @details Strutture dati della board su cui montare i devices ed i segnali
 */

#include "board.h"

/** @brief      Board create
 *  @details    Crea una board ed inizializza i membri della struct
 *  @param      **b Indirizzo del puntatore a t_board
 *  @return     *b  Puntatore a t_board
 */
void board_create( t_board **b ) {
  *b = ( t_board * ) calloc( 1, sizeof( t_board ) );
  if ( *b != NULL ) {
    //  Inizio con tutti i device in pausa
    (( t_board * )( *b ))->run_mode = rm_pause;
    //  Inizializzo lista wiress
    wires_create( &(( t_board * )( *b ))->wires );
    //  Inizializzo lista devices
    list_create( &(( t_board * )( *b ))->devices );
  }
}

/** @brief      Plug Device
 *  @details    Inserisce il device nella board, collega i puntatori delle strutture dati ed inizializza il device
 *  @param      *b  Puntatore a t_board
 *  @param      *d  Puntatore a t_device
 */
void board_plug_device( t_board *b, t_device *d ) {
  t_devices_node         *n = NULL;
  unsigned int        i_sig = 0;
  t_signal      *ret_signal = NULL;

  // Aggiungere test b->devices != NULL ??
  // Aggiungere test d non presente nella lista devices ??
  if ( b != NULL && d != NULL ) {
    //  Registro il device nella board
    node_create( &n );
    n->data = d;
    list_insert( b->devices, n, -1 );

    //  Collego il device alla board
    for ( i_sig = 0; i_sig < d->internal.sig_desc.sig_num; i_sig++ ) {
      wires_get_signal( b->wires, d->internal.sig_desc.sig_names[ i_sig ], &ret_signal );
      if ( ret_signal == NULL ) {
        signal_create( &ret_signal, d->internal.sig_desc.sig_names[ i_sig ] );
        if ( ret_signal != NULL ) {
          wires_add_signal( b->wires, ret_signal );
          //  Collego il segnale al device
          device_link_signal( d, i_sig, ret_signal );
        }
      }
      
    }

    //  Inizializzo device
    d->init( d );
  }
}

/** @brief      Set run state
 *  @details    Imposta il run state della board e di tutti i devices montati
 *  @param      *b  Puntatore a t_board
 *  @param      rm  Run state
 */
void board_run_state( t_board *b, enum e_run_mode rm ) {
  t_devices_node    *n = NULL;
  t_device          *d = NULL;

  b->run_mode = rm;
  if ( b->devices != NULL ) {
    for ( n = b->devices->first; n != NULL; n = n->next ) {
      d = n->data;
      if ( d != NULL )
        d->internal.run_mode = rm;
    }
  }
}
