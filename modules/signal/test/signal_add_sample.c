#include <stdlib.h>
#include <stdio.h>
#include "list.h"
#include "signal.h"

void dump_sample( t_node* node ) {
  t_sample* s = NULL;
  if ( node != NULL ) {
    s = ( t_sample* )node->data;
    if ( s != NULL && s->sample_value != NULL ) {
      printf( "    Sample ID   : %d\tSample Value: ( %16p -> %d )", ( int ) s->sample_id, s->sample_value, *( int* )s->sample_value );
    } else {
      printf( "    Sample ID   : %d\tSample Value: %16p", ( int ) s->sample_id, s->sample_value );
    }
  }
}

int main( int argc, char **argv ) {
  int const lo  = 0;
  int const hi  = 1;
  t_signal*  s  = NULL;
  t_sample* sd  = NULL;

  printf( "\n" );
  printf( "\nCostante LO: ( %16p -> %1d )", &lo, lo );
  printf( "\nCostante HI: ( %16p -> %1d )", &hi, hi );

  signal_create( &s, "CLOCK" );

  if ( s->data != NULL ) {
  printf( "\nSegnale: %s", ( char* )( ( t_signal_data* )s->data )->name );
  }
  list_dump( s, NULL, dump_sample, "\n  ", NULL );
 
  printf( "\nAggiungo sample" );
  sd = NULL;
  signal_add_sample( s, 0, ( int*)&lo, &sd ); 
  if ( sd != NULL ) {
  printf( "  ( %16p ), sample_id: %4d", sd, sd->sample_id );
  if ( sd->sample_value != NULL )
  printf( "  sample_value: ( %16p -> %4d )", sd->sample_value, *( int * )sd->sample_value );
  }

  printf( "\nAggiungo sample" );
  sd = NULL;
  signal_add_sample( s, 1, ( int*)&hi, &sd ); 
  if ( sd != NULL ) {
  printf( "  ( %16p ), sample_id: %4d", sd, sd->sample_id );
  if ( sd->sample_value != NULL )
  printf( "  sample_value: ( %16p -> %4d )", sd->sample_value, *( int * )sd->sample_value );
  }
 
  printf( "\nAggiungo sample" );
  sd = NULL;
  //    Test valore di ritorno del t_sample* creato opzionale, passo NULL come 4° parametro.
  signal_add_sample( s, 2, ( int*)&lo, NULL ); 
  if ( sd != NULL ) {
  printf( "  ( %16p ), sample_id: %4d", sd, sd->sample_id );
  if ( sd->sample_value != NULL )
  printf( "  sample_value: ( %16p -> %4d )", sd->sample_value, *( int * )sd->sample_value );
  }

  list_dump( s, NULL, dump_sample, "\n  ", NULL );

  printf( "\nOK.\n" );

  return 0;
}
