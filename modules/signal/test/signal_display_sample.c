#include <stdlib.h>
#include <stdio.h>
#include "list.h"
#include "signal.h"

void cb_display_continue( t_sample *sam, t_samp_width p_width, char *ret ) {
  if( ret == NULL )
    return;

  if ( *( ( int*)sam->sample_value ) == 0 )
    memset( ret, 's', p_width );
  if ( *( ( int*)sam->sample_value ) == 1 )
    memset( ret, 'S', p_width );
  *(ret + p_width) = '\0';
}

void cb_display_skip( t_sample *sam, t_samp_width p_width, char *ret ) {
  if( ret == NULL )
    return;

  memset( ret, 'e', p_width );
  *(ret + p_width) = '\0';
}

void cb_display_data( t_sample *sam, t_samp_width p_width, char *ret ) {
  if( ret == NULL )
    return;

  if ( *( ( int*)sam->sample_value ) == 0 )
    memset( ret, '_', p_width );
  if ( *( ( int*)sam->sample_value ) == 1 )
    memset( ret, '"', p_width );
  *(ret + p_width) = '\0';
}

void cb_display_change( t_sample *sam, t_samp_width p_width, char *ret ) {
  if( ret == NULL )
    return;

  *ret = '|';
  if ( *( ( int*)sam->sample_value ) == 0 )
    memset( ret+1, '_', p_width-1 );
  if ( *( ( int*)sam->sample_value ) == 1 )
    memset( ret+1, '"', p_width-1 );
  *(ret + p_width) = '\0';
}

int main( int argc, char **argv ) {
  int const          lo = 0;
  int const          hi = 1;
  t_signal           *s = NULL;
  char            *wave = NULL;
  t_wave_width    width = 0;

  printf( "\n" );

  signal_create( &s, "CLOCK" );

  signal_add_sample( s, 0, ( int*)&lo, NULL ); 
  signal_add_sample( s, 1, ( int*)&hi, NULL ); 
  signal_add_sample( s, 2, ( int*)&lo, NULL ); 
  signal_add_sample( s, 3, ( int*)&hi, NULL ); 
  signal_add_sample( s, 4, ( int*)&lo, NULL ); 
  signal_add_sample( s, 5, ( int*)&hi, NULL ); 
  signal_add_sample( s, 6, ( int*)&lo, NULL ); 

  //signal_display_create( &( ( t_signal_data* ) s->data )->display, 2, cb_display_data, cb_display_change, NULL, NULL );
  signal_display_create( &( ( t_signal_data* ) s->data )->display, 2, cb_display_data, cb_display_data, cb_display_skip, cb_display_continue );
  // Riservo 127 byte
  signal_display_start( s, 127 );

  signal_display_sample( s, 5, display_data );
  signal_display_sample( s, 5, display_data );
  signal_display_sample( s, 5, display_data );
  signal_display_sample( s, 5, display_data );
  signal_display_sample( s, 5, display_data );
  signal_display_sample( s, 5, display_data );
  signal_display_sample( s, 5, display_data );

  // Estrazione stringa
  signal_display_get( s, &wave, &width );
  printf( "%s\n", wave );

  // Test realloc
  signal_display_start( s, 255 );

  signal_display_sample( s, 5, display_data );
  signal_display_sample( s, 5, display_data );
  signal_display_sample( s, 5, display_data );
  signal_display_sample( s, 5, display_data );

  signal_display_sample( s, 5, display_skip );
  
  signal_display_sample( s, 5, display_data );

  signal_display_sample( s, 5, display_same );

  signal_display_sample( s, 5, display_data );

  signal_display_sample( s, 5, display_same );

  signal_display_sample( s, 5, display_data );

  // Estrazione stringa
  signal_display_get( s, &wave, &width );
  printf( "%s\n", wave );

  printf( "\nOK.\n" );

  return 0;
}
