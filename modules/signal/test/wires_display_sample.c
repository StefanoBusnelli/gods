#include <stdlib.h>
#include <stdio.h>
#include "list.h"
#include "signal.h"

typedef const unsigned int cu_int;
typedef const short unsigned int csu_int;

//  --------------------------------------------------------------

void digi_data( t_sample *sam, t_samp_width p_width, char *ret ) {
  if( ret == NULL )
    return;

  if ( *( ( cu_int*)sam->sample_value ) == 0 )
    memset( ret, '_', p_width );
  if ( *( ( cu_int*)sam->sample_value ) == 1 )
    memset( ret, '^', p_width );

  if ( *( ( cu_int*)sam->sample_value ) == 0 )
    *(ret + p_width-1) = '.';
  if ( *( ( cu_int*)sam->sample_value ) == 1 )
    *(ret + p_width-1) = '\'';

  *(ret + p_width)   = '\0';
}

void digi_change( t_sample *sam, t_samp_width p_width, char *ret ) {
  if( ret == NULL )
    return;

  *ret = '|';
  if ( *( ( cu_int*)sam->sample_value ) == 0 )
    memset( ret+1, '_', p_width-1 );
  if ( *( ( cu_int*)sam->sample_value ) == 1 )
    memset( ret+1, '^', p_width-1 );

  if ( *( ( cu_int*)sam->sample_value ) == 0 )
    *(ret + p_width-1) = '.';
  if ( *( ( cu_int*)sam->sample_value ) == 1 )
    *(ret + p_width-1) = '\'';

  *(ret + p_width)   = '\0';
}

void digi_cont( t_sample *sam, t_samp_width p_width, char *ret ) {
  if( ret == NULL )
    return;

  if ( *( ( cu_int*)sam->sample_value ) == 0 )
    memset( ret, '_', p_width );
  if ( *( ( cu_int*)sam->sample_value ) == 1 )
    memset( ret, '^', p_width );
  
  if ( *( ( cu_int*)sam->sample_value ) == 0 )
    *(ret + p_width-1) = '.';
  if ( *( ( cu_int*)sam->sample_value ) == 1 )
    *(ret + p_width-1) = '\'';

  *(ret + p_width)   = '\0';
}

void digi_skip( t_sample *sam, t_samp_width p_width, char *ret ) {
  if( ret == NULL )
    return;

  memset( ret, ' ', p_width );

  if ( *( ( cu_int*)sam->sample_value ) == 0 )
    *(ret + p_width-1) = '.';
  if ( *( ( cu_int*)sam->sample_value ) == 1 )
    *(ret + p_width-1) = '\'';

  *(ret + p_width)   = '\0';
}

//  --------------------------------------------------------------

void bus_data( t_sample *sam, t_samp_width p_width, char *ret ) {
  if( ret == NULL )
    return;

  sprintf( ret, "= %04x :", *(csu_int*)sam->sample_value );
  *(ret + p_width) = '\0';
}

void bus_change( t_sample *sam, t_samp_width p_width, char *ret ) {
  if( ret == NULL )
    return;

  sprintf( ret, "x %04x :", *(csu_int*)sam->sample_value );
  *(ret + p_width) = '\0';
}

void bus_cont( t_sample *sam, t_samp_width p_width, char *ret ) {
  if( ret == NULL )
    return;

  memset( ret, '=', p_width );
  *(ret + p_width-1) = ':';
  *(ret + p_width)   = '\0';
}

void bus_skip( t_sample *sam, t_samp_width p_width, char *ret ) {
  if( ret == NULL )
    return;

  memset( ret, ' ', p_width );
  *(ret + p_width-1) = ':';
  *(ret + p_width)   = '\0';
}

//  --------------------------------------------------------------

int main( int argc, char **argv ) {
  cu_int             lo = 0;
  cu_int             hi = 1;
  csu_int         dbus0 = 0x0000; 
  csu_int         dbus1 = 0xC3B2; 
  t_wires            *w = NULL;
  t_wires_node      *wn = NULL;
  t_signal         *sda = NULL;
  t_signal         *scl = NULL;
  t_signal         *bus = NULL;
  t_signal           *s = NULL;
  t_signal_data    *sdt = NULL;
  char            *wave = NULL;
  t_wave_width    width = 0;

  printf( "\n" );

  // Creo segnali
  wires_create( &w );
  signal_create( &sda, "SDA" );
  signal_create( &scl, "SCL" );
  signal_create( &bus, "BUS" );
  wires_add_signal( w, sda );
  wires_add_signal( w, scl );
  wires_add_signal( w, bus );

  // Creo display per i segnali
  signal_display_create( \
    &( ( t_signal_data* ) sda->data )->display, \
    3, \
    digi_data, \
    digi_change, \
    digi_skip, \
    digi_cont \
  );
  signal_display_create( \
    &( ( t_signal_data* ) scl->data )->display, \
    3, \
    digi_data, \
    digi_change, \
    digi_skip, \
    digi_cont \
  );
  signal_display_create( \
    &( ( t_signal_data* ) bus->data )->display, \
    8, \
    bus_data, \
    bus_change, \
    bus_skip, \
    bus_cont \
  );

  // Init
  wires_add_signal_sample( scl, ( void* )&hi, false );       //  0
  wires_add_signal_sample( sda, ( void* )&hi, false );       //  0
  wires_add_signal_sample( bus, ( void* )&dbus0, false );    //  0
  // Start
  wires_add_signal_sample( sda, ( void* )&lo, true );        //  1
  wires_add_signal_sample( scl, ( void* )&lo, true );        //  2
  // 1
  wires_add_signal_sample( sda, ( void* )&hi, true );        //  3
  wires_add_signal_sample( scl, ( void* )&hi, true );        //  4
  wires_add_signal_sample( scl, ( void* )&lo, true );        //  5
  wires_add_signal_sample( sda, ( void* )&lo, true );        //  6
  // data bus
  wires_add_signal_sample( bus, ( void* )&dbus1, true );     //  7
  // Stop
  wires_add_signal_sample( scl, ( void* )&hi, true );        //  8
  wires_add_signal_sample( sda, ( void* )&hi, true );        //  9
  
  // Genero waves
  wires_display_sample( w, 127 );

  // Visualizzo
  printf( "\nEsito.\n" );
  for ( wn = w->first; wn != NULL; wn = wn->next ) {
    s = wn->data;
    sdt = s->data;
    signal_display_get( s, &wave, &width );
    printf( "%8s %s\n", sdt->name, wave );
  }
  printf( "\nOK.\n" );

  return 0;
}
