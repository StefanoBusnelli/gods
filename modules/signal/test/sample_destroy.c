#include <stdlib.h>
#include <stdio.h>
#include "list.h"
#include "signal.h"

int main( int argc, char **argv ) {
  t_signal* s   = NULL;

  printf( "\n" );

  printf( "\nPuntatore a costante" );
  //    Utilizzo delle costanti e faccio puntare tutti i sample_value a queste evitando di allocare
  //        memoria per memorizzare gli stessi valori ( 0, 1 )
  int const lo  = 0;
  int const hi  = 1;
  //    aggiorno il valore di sample_value 
  printf( "\nint const lo: %16p %16d", &lo, lo );
  printf( "\nint const hi: %16p %16d", &hi, hi );

  signal_create( &s, "CLOCK" );
  signal_add_sample( s, 0, ( int* )&hi, NULL ); 

  printf( "\nSignal (t_signal):                       %16p %4ld bytes %4ld bytes", s, sizeof( s ), sizeof( *s ) );

  printf( "\nSignal->nodes:                           %16d %4ld bytes", s->nodes, sizeof( s->nodes ) );
  printf( "\nSignal->data:                            %16p %4ld bytes", s->data, sizeof( s->data ) );
  if ( s->data != NULL ) {
  printf( "\n    %s", ( char* )s->data );
  }

  printf( "\nSignal->first (t_signal_node):           %16p %4ld bytes", s->first, sizeof( s->first ) );
  if ( s->first != NULL ) {
  printf( "\nSignal->first->data (t_sample):          %16p %4ld bytes", s->first->data, sizeof( s->first->data ) );
  if ( s->first->data != NULL ) {
  printf( "\nSignal->first->data->sample_id:          %16d", (( t_sample* )(s->first->data))->sample_id );
  if ( (( t_sample* )(s->first->data))->sample_value != NULL ) {
  printf( "\nSignal->first->data->sample_value:     ( %16p -> %4d)", (( t_sample* )(s->first->data))->sample_value, *( int* )( (( t_sample* )(s->first->data))->sample_value ) );
  } else {
  printf( "\nSignal->first->data->sample_value:       %16p", (( t_sample* )(s->first->data))->sample_value );
  }
  }

  printf( "\nSignal->first->prev:                     %16p %4ld bytes", s->first->prev, sizeof( s->first->prev ) );
  printf( "\nSignal->first->next:                     %16p %4ld bytes", s->first->next, sizeof( s->first->next ) );
  }

  list_dump( s, NULL, NULL, "\n  ", NULL );

  int* sample = NULL;
  printf( "\nCancello i dati del sample" );
  sample_destroy( ( t_sample** )&s->first->data, ( void** )&sample );
  printf( "\nSignal->first->data (t_sample):          %16p", s->first->data );

  if ( s->first->data != NULL ) {
  printf( "\nDato non eliminato" );
  } else {
  printf( "\nDato eliminato" );
  }

  printf( "\nValore sample:                         ( %16p -> %4d )", sample, *sample );
  if ( sample != NULL ) {
  printf( "\nValore di sample da eliminare ( se creato con malloc ) " );
  } else {
  printf( "\nNessun valore di sample presente" );
  }

  list_dump( s, NULL, NULL, "\n  ", NULL );

  printf( "\nOK.\n" );

  return 0;
}
