#include <stdlib.h>
#include <stdio.h>
#include "list.h"
#include "signal.h"

int main( int argc, char **argv ) {
  t_signal* s   = NULL;
  t_sample* sd  = NULL;

  printf( "\n" );

  printf( "Puntatore a costante\n" );
  //    Utilizzo delle costanti e faccio puntare tutti i sample_value a queste evitando di allocare
  //        memoria per memorizzare gli stessi valori ( 0, 1 )
  int const lo  = 0;
  int const hi  = 1;
  //    aggiorno il valore di sample_value 
  printf( "int const lo: %16p %16d\n", &lo, lo );
  printf( "int const hi: %16p %16d\n", &hi, hi );

  signal_create( &s, "VCC" );
  signal_add_sample( s, 0, ( int* )&hi, &sd );
 
  printf( "\n" );
  printf( "Signal (t_signal):                       %16p %16ld bytes %16ld bytes\n", s, sizeof( s ), sizeof( *s ) );

  printf( "Signal->nodes:                           %16d %16ld bytes\n", s->nodes, sizeof( s->nodes ) );
  printf( "Signal->data:                            %16p %16ld bytes\n", s->data, sizeof( s->data ) );
  if ( s->data != NULL ) {
  printf( "    %s\n", ( char* )s->data );
  }

  printf( "\n" );
  printf( "Signal->first (t_signal_node):           %16p %16ld bytes\n", s->first, sizeof( s->first ) );
  if ( s->first != NULL ) {

  printf( "Signal->first->data (t_sample):          %16p %16ld bytes\n", s->first->data, sizeof( s->first->data ) );
  if ( s->first->data != NULL ) {

  printf( "Signal->first->data->sample_id:          %16d\n", (( t_sample* )(s->first->data))->sample_id );
  printf( "Signal->first->data->sample_value:       %16p\n", (( t_sample* )(s->first->data))->sample_value );
  if ( (( t_sample* )(s->first->data))->sample_value != NULL ) {
  printf( "                                         %16d\n", *( int* )( (( t_sample* )(s->first->data))->sample_value ) );
  }

  printf( "\nModifico i dati del sample\n" );
  sample_set( ( t_sample* )s->first->data, 1, ( int* )&lo );

  printf( "Signal->first->data->sample_id:          %16d\n", (( t_sample* )(s->first->data))->sample_id );
  printf( "Signal->first->data->sample_value:       %16p\n", (( t_sample* )(s->first->data))->sample_value );
  if ( (( t_sample* )(s->first->data))->sample_value != NULL ) {
  printf( "                                         %16d\n", *( int* )( (( t_sample* )(s->first->data))->sample_value ) );
  }

  printf( "\nModifico i dati del sample con malloc\n" );
  int* v = ( int* )calloc( 1, sizeof( int ) );
  *v = 42;
  sample_set( ( t_sample* )s->first->data, 2, v );

  printf( "Signal->first->data->sample_id:          %16d\n", (( t_sample* )(s->first->data))->sample_id );
  printf( "Signal->first->data->sample_value:       %16p\n", (( t_sample* )(s->first->data))->sample_value );
  if ( (( t_sample* )(s->first->data))->sample_value != NULL ) {
  printf( "                                         %16d\n", *( int* )( (( t_sample* )(s->first->data))->sample_value ) );
  }

  }

  }

  return 0;
}
