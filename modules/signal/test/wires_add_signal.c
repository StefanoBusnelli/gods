#include <stdlib.h>
#include <stdio.h>
#include "list.h"
#include "signal.h"

int main( int argc, char **argv ) {
  t_wires*  w       = NULL;
  t_signal* s_clk   = NULL;

  wires_create( &w );

  signal_create( &s_clk, "CLOCK" );

  wires_add_signal( w, s_clk );

  printf( "\nOK.\n" );

  return 0;
}
