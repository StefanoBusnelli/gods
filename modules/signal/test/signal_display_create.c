#include <stdlib.h>
#include <stdio.h>
#include "list.h"
#include "signal.h"

int main( int argc, char **argv ) {
  t_signal* s   = NULL;

  printf( "\n" );

  signal_create( &s, "VCC" );
  signal_display_create( &( ( t_signal_data* ) s->data )->display, 1, NULL, NULL, NULL, NULL );

  printf( "\nOK.\n" );

  return 0;
}
