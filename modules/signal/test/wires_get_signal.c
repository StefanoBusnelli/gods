#include <stdlib.h>
#include <stdio.h>
#include "list.h"
#include "signal.h"

int main( int argc, char **argv ) {
  t_wires*  w       = NULL;
  t_signal* s_gnd   = NULL;
  t_signal* s_clk   = NULL;
  t_signal* r       = NULL;

  wires_create( &w );

  signal_create( &s_gnd, "GND" );
  signal_create( &s_clk, "CLOCK" );
  wires_add_signal( w, s_gnd );
  wires_add_signal( w, s_clk );

  wires_get_signal( w, "GND", &r );
  wires_get_signal( w, "CLOCK", &r );
  wires_get_signal( w, "VCC", &r );

  printf( "\nOK.\n" );

  return 0;
}
