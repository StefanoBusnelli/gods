#include <stdlib.h>
#include <stdio.h>
#include "list.h"
#include "signal.h"

int main( int argc, char **argv ) {
  t_wires*  w       = NULL;
  t_signal* s_clk   = NULL;
  unsigned int const gnd = 0;
  unsigned int const vcc = 5;

  wires_create( &w );

  signal_create( &s_clk, "CLOCK" );

  wires_add_signal( w, s_clk );
  ( ( t_wires_data* )w->data )->max_samples_signal = 8;

  wires_add_signal_sample( s_clk, ( unsigned int* )&gnd, true );
  wires_add_signal_sample( s_clk, ( unsigned int* )&vcc, true );
  wires_add_signal_sample( s_clk, ( unsigned int* )&gnd, true );
  wires_add_signal_sample( s_clk, ( unsigned int* )&vcc, true );
  wires_add_signal_sample( s_clk, ( unsigned int* )&gnd, true );
  wires_add_signal_sample( s_clk, ( unsigned int* )&vcc, true );
  wires_add_signal_sample( s_clk, ( unsigned int* )&gnd, true );
  wires_add_signal_sample( s_clk, ( unsigned int* )&vcc, true );

  wires_add_signal_sample( s_clk, ( unsigned int* )&gnd, true );
  wires_add_signal_sample( s_clk, ( unsigned int* )&vcc, true ); 

  printf( "\nOK.\n" );

  return 0;
}
