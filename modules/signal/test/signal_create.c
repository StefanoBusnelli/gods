#include <stdlib.h>
#include <stdio.h>
#include "list.h"
#include "signal.h"

void dump_sample( t_node* node ) {
  t_sample* s = NULL;
  if ( node != NULL ) {
    s = ( t_sample* )node->data;
    if ( s != NULL && s->sample_value != NULL ) {
      printf( "    Sample ID   : %d\tSample Value: ( %16p -> %d )", ( int ) s->sample_id, s->sample_value, *( int* )s->sample_value );
    } else {
      printf( "    Sample ID   : %d\tSample Value: %16p", ( int ) s->sample_id, s->sample_value );
    }
  }
}

int main( int argc, char **argv ) {
  t_signal* s   = NULL;

  printf( "\n" );

  signal_create( &s, "VCC" );

  printf( "\nSignal (t_signal):                       %16p %16ld bytes %16ld bytes", s, sizeof( s ), sizeof( *s ) );

  printf( "\nSignal->nodes:                           %16d %16ld bytes", s->nodes, sizeof( s->nodes ) );
  printf( "\nSignal->data:                            %16p %16ld bytes", s->data, sizeof( s->data ) );
  if ( s->data != NULL ) {
  printf( "\n    %s", ( char* )( ( t_signal_data* )s->data )->name );
  }
  printf( "\nSignal->first (t_signal_node):           %16p %16ld bytes", s->first, sizeof( s->first ) );
  if ( s->first != NULL ) {
  printf( "\nSignal->first->data (t_sample):          %16p %16ld bytes", s->first->data, sizeof( s->first->data ) );
  if ( s->first->data != NULL ) {
  printf( "\nSignal->first->data->sample_id:          %16d", (( t_sample* )(s->first->data))->sample_id );
  printf( "\nSignal->first->data->sample_value:       %16p", (( t_sample* )(s->first->data))->sample_value );
  if ( (( t_sample* )(s->first->data))->sample_value != NULL ) {
  printf( "\n                                         %16d", *( int* )( (( t_sample* )(s->first->data))->sample_value ) );
  }
  }
  printf( "\nSignal->first->prev:                     %16p %16ld bytes", s->first->prev, sizeof( s->first->prev ) );
  printf( "\nSignal->first->next:                     %16p %16ld bytes", s->first->next, sizeof( s->first->next ) );
  }
  printf( "\nSignal->last (t_signal_node):            %16p %16ld bytes", s->last, sizeof( s->last ) );

  printf( "\nDump lista Signal:" );
  list_dump( s, NULL, dump_sample, "\n  ", NULL );

  printf( "\nOK.\n" );

  return 0;
}
