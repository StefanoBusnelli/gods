#include <stdlib.h>
#include <stdio.h>
#include "list.h"
#include "signal.h"

int main( int argc, char **argv ) {
  t_signal* s   = NULL;
  char*     n   = NULL;

  signal_create( &s, "GND" );
  list_dump( s, NULL, NULL, "\n  ", NULL );

  signal_get_name( s, &n );
  printf( "\nNome: %s", n );

  printf( "\nOk.\n" );

  return 0;
}
