/**
 * @file    signal.c
 * @author   Stefano Busnelli    busnelli.stefano@gmail.com
 * @version  1.3.0
 * @brief    Segnali
 */

#include "signal.h"

//  --------------------------------------------------------------------------------
//  Metodi di gestione dei samples

/** @brief      Creazione sample
 *  @details    Assegna memoria per la struct t_sample ed inizializza i valori
 *  @param      **sd    Indirizzo del puntatore a t_sample
 *  @param      s_id    Sample id
 *  @param      *s_val  Puntatore al valore da inserire nel sample
 *  @returns    *sd     Puntatore a t_sample
 */
void sample_create( t_sample **sd, t_sample_id s_id, void *s_val ) {
  t_sample  *sam = NULL;
  if ( sd != NULL ) {
    sam = ( t_sample* ) calloc( 1, sizeof( t_sample ) );
    if ( sam != NULL ) {
      sam->sample_id       = s_id;
      sam->sample_value    = s_val;
    }
    *sd = sam;
  }
}

/** @brief      Distruzione sample
 *  @details    Libera la memoria utilizzata per la struct t_sample e restituisce il puntatore al valore inserito nel sample per una aventuale freei. Assegna a NULL il puntatore al t_sample.
 *  @param      **sd    Indirizzo del puntatore a t_sample
 *  @param      **sv    Indirizzo del puntatore al valore
 *  @returns    *sd     Puntatore a t_sample
 *  @returns    *sv     Puntatore al valore memorizzato nel sample
 */
void sample_destroy( t_sample **sd, void **sv ) {
  if ( *sd != NULL ) {
    // Restituisco i dati puntati dal sample perchè non posso sapere a questo livello se devono essere disallocati dalla memoria
    if ( sv != NULL )
      *sv = (( t_sample* )( *sd ))->sample_value;
    // Libero la memoria allocata dal sample
    free( *sd );
    *sd = NULL;
  }
}

/** @brief      Assegnamento valori sample
 *  @details    Assegna i valori ad un sample
 *  @param      *sd     Puntatore a t_sample
 *  @param      s_id    sample id
 *  @param      *s_val  Puntatore al valore da inserire nel sample
 */
void sample_set( t_sample *sd, t_sample_id s_id, void *s_val ) {
  /*
    TODO:   Testare anche il valore di ->sample_value che dovrebbe essere NULL prima di poter assegnare un nuovo indirizzo.
            Questo per evitare che venga perso il riferimento ad un'area di memodia allocata che non potrà essere più liberata.
  */
  if ( sd != NULL ) {
    sd->sample_id       = s_id;
    sd->sample_value    = s_val;
  }
}

/** @brief      Inizializzazione valori sample
 *  @details    Azzera sample_id ed elimina sample_value liberando la memoria.
 *  @param      *sd Puntatore a t_sample
 */
void sample_clear( t_sample *sd ) {
  if ( sd != NULL ) {
    sd->sample_id       = 0;
    free( sd->sample_value );
    sd->sample_value    = NULL;
  }
}

//  --------------------------------------------------------------------------------
//  Metodi di gestione dei segnali

/**
 *  @brief      Creazione del segnale vuoto
 *  @details    Riserva la memoria per un segnale ed inizializza tutti i valori assegnando il nome
 *  @param      **s             Indirizzo del puntatore al segnale t_signal
 *  @param      *signal_name    Puntatore a signal_name ( char* )
 *  @return     *s              Puntatore a t_signal
 *  @code
 *  t_signal *s;
 *  signal_create( &s, "Vcc" );
 *  @endcode
 */
void signal_create( t_signal **s, char *signal_name ) {
  t_signal_data *s_data = NULL;

  list_create( s );
  if ( *s != NULL ) {
    s_data = ( t_signal_data* ) calloc( 1, sizeof( t_signal_data ) );
    if ( s_data != NULL ) {
      s_data->name      = signal_name;
      s_data->wires     = NULL;
      s_data->display   = NULL;
    }
    ( ( t_signal* ) *s)->data = s_data;
  }
}

/**
 *  @brief      Lettura nome del segnale
 *  @details    Restituisce la stringa contenente il nome del segnale
 *  @param      *s              Puntatore al segnale t_signal
 *  @param      **signal_name   Indirizzo del puntatore a signal_name ( char** )
 *  @return     *signal_name    Puntatore a char *
 *  @code
 *  t_signal *s;
 *  char     *n;
 *  signal_create( &s, "Vcc" );
 *  signal_get_name( s, &n );
 *  printf( "%s", n );
 *  @endcode
 */
void signal_get_name( t_signal *s, char **signal_name ) {
  t_signal_data *sd = NULL;
  if ( s != NULL ) {
    sd = s->data; 
    *signal_name = ( char* )sd->name;
  }
}

/**
 *  @brief      Aggiunta di un sample al segnale
 *  @details    Crea un sample con i valori, lo aggiunge al segnale e ne restituisce l'indirizzo.
 *  @param      *s              Puntatore al segnale t_signal
 *  @param      s_id            Sample id ( Vedi sample_create )
 *  @param      *s_val          Indirizzo del valore da inserire ( Vedi sample_create )
 *  @param      **ret_sd        Indirizzo del puntatore a t_sample
 *  @return     *ret_sd         Puntatore a t_sample
 *  @code
 *  @endcode
 */
void signal_add_sample( t_signal *s, t_sample_id s_id, void *s_val, t_sample **ret_sd ) {
  t_signal_node*    n = NULL;
  t_sample*        sd = NULL;

  if ( s != NULL ) {
    node_create( &n );
    if ( n != NULL ) {
      sample_create( &sd, s_id, s_val );
      if ( sd != NULL ) {
        n->data = sd;
        list_insert( s, n, -1 );
        if ( ret_sd != NULL ) {
          *ret_sd = sd;
        }
      }
    }
  }
}

/**
 *  @brief      Creazione struttura dati display sample
 *  @details    Riserva la memoria per la struttura dati da assegnare a t_signal_data->display e ne inizializza i membri
 *  @param      **sd        Indirizzo del puntatore alla struct da creare
 *  @param      p_swidth    t_signal_display->samp_width
 *  @param      p_data      Puntatore a callback per t_signal_display->data     ( Anche NULL ) 
 *  @param      p_change    Puntatore a callback per t_signal_display->change   ( Anche NULL ) 
 *  @param      p_skip      Puntatore a callback per t_signal_display->skip     ( Anche NULL )
 *  @param      p_same      Puntatore a callback per t_signal_display->same     ( Anche NULL )
 *  @return     *sd         Puntatore a t_signal_display
 *  @code
 *  t_signal *s;
 *  signal_create( &s, "Vcc" );
 *  signal_display_create( &( ( t_signal_display* ) s->data )->display, 1, NULL, NULL, NULL );
 *  @endcode
 */
void signal_display_create( t_signal_display **sd, t_samp_width p_swidth, f_display_sample p_data, f_display_sample p_change, f_display_sample p_skip, f_display_sample p_same ) {
  t_signal_display  *s_disp = NULL;
  if ( sd != NULL ) {
    s_disp = ( t_signal_display *) calloc( 1, sizeof( t_signal_display ) );
    if ( s_disp != NULL ) {
      s_disp->samp_dim    = p_swidth;
      s_disp->samp_width  = 0;
      s_disp->samp_buffer = NULL;
      s_disp->wave_width  = 0;
      s_disp->wave_buffer = NULL;
      s_disp->wave_length = 0;
      s_disp->last_node   = NULL;
      s_disp->curr_node   = NULL;
      s_disp->data        = p_data;
      s_disp->change      = p_change;
      s_disp->skip        = p_skip;
      s_disp->same        = p_same;
    }
    *sd = s_disp;
  }
}

/**
 *  @brief      Inizio composizione forma d'onda del segnale
 *  @details    Riserva lo spazio per il buffer ->wave_buffer ed inizializza tutti i valori
 *              Inizializza o ri-inizializza i valori ogni volta che occorre rigenerare la forma d'onda del segnale
 *  @param      *s          Indirizzo del puntatore alla struct da creare
 *  @param      p_wwidth    Dimensioni stringa t_signal_display->wave_width
 *  @code
 *  t_signal *s;
 *  signal_create( &s, "Vcc" );
 *  signal_display_create( &( ( t_signal_display* ) s->data )->display, 1, NULL, NULL, NULL );
 *  @endcode
 */
void signal_display_start( t_signal *s, t_wave_width p_wwidth ) {
  t_signal_data     *sig_data   = NULL;
  t_signal_display  *sig_disp   = NULL;

  if ( s == NULL )
    return;
  
  sig_data = s->data;

  if ( sig_data == NULL )
    return;
  
  sig_disp = sig_data->display;

  if ( sig_disp == NULL ) 
    return;

  if ( sig_disp->wave_width == 0 ) {
    sig_disp->wave_buffer = ( char* ) calloc( ( p_wwidth+1 ), sizeof( char ) );
  } else {
    sig_disp->wave_buffer = ( char* ) realloc( sig_disp->wave_buffer, sizeof( char ) * ( p_wwidth + 1 ) );
    if ( sig_disp->wave_buffer != NULL )
      memset( sig_disp->wave_buffer, '\0', sig_disp->wave_width+1 ) ;
  }
  sig_disp->wave_width  = p_wwidth;
  sig_disp->wave_length = 0;
  sig_disp->last_node   = NULL;
  sig_disp->curr_node   = s->first;
}

/**
 *  @brief      Generazione della forma d'onda del segnale
 *  @details    Produce la forma d'onda per il campione corrente ed accoda il risultato alla forma d'onda del segnale.
                Memorizza tutte le informazioni sullo stato nella struttura dati signal_display in modo tale che la funzione possa essere richiamata per passi successivi
 *  @param      *s          Puntatore al segnale
 *  @param      p_swidth    Dimensione in caratteri della rappresentazione del sample da passare alle call back
 *  @param      p_mode      display_data        Converte in lavore del sample in in stringa e passa al nodo successivo
 *                          display_skip        Inserisce spazio vuoto
 *                          display_same        Continua a visualizzare il valore dell' ultimo sample
 *  @code
 *  t_signal *s;
 *  signal_create( &s, "Vcc" );
 *  signal_display_create( &( ( t_signal_display* ) s->data )->display, 1, NULL, NULL, NULL );
 *  @endcode
 *  @todo       Testare modalità display_skip
 */
void signal_display_sample( t_signal *s, t_samp_width p_swidth, enum e_signal_display_mode p_mode ) {
  t_signal_data     *sig_data   = NULL;
  t_signal_display  *sig_disp   = NULL;
  t_sample          *sam_cur    = NULL;
  t_sample          *sam_pre    = NULL;

  // Segnale inesistente
  if ( s == NULL )
    return; 

  // Dati inesistenti
  if ( s->data == NULL ) 
    return;

  // Nodi inesistenti
  if ( s->first == NULL ) 
    return;

  if ( s->last == NULL ) 
    return;

  sig_data = s->data;

  // Dati display inesistenti
  if ( sig_data->display == NULL ) 
    return;

  sig_disp = sig_data->display;
  
  // Prima esecuzione di display_sample, non c'è un nodo visualizzato quindi mi posiziono sul primo
  if ( sig_disp->last_node == NULL && sig_disp->curr_node == NULL )
    sig_disp->curr_node = s->first;

  // Validazione p_mode == display_data
  if ( p_mode == display_data     && sig_disp->curr_node == NULL )
    return;
  // Validazione p_mode == display_same
  if ( p_mode == display_same && sig_disp->last_node == NULL )
    return;

  // Controllo che in wave_buffer ci sia abbastanza spazio per il nuovo samp_buffer
  if ( sig_disp->wave_length + p_swidth > sig_disp->wave_width )
    return;

  // Inizializzo il buffer per rappresentare il campione
  if ( sig_disp->samp_width == 0 ) {
    sig_disp->samp_width  = p_swidth;
    sig_disp->samp_buffer = ( char* ) calloc( ( p_swidth+1 ), sizeof( char ) );
  } else {
    if ( sig_disp->samp_width != p_swidth ) {
      sig_disp->samp_width  = p_swidth;
      sig_disp->samp_buffer = ( char* ) realloc( sig_disp->samp_buffer, sizeof( char ) * ( p_swidth + 1 ) );
    }
  }
  if ( sig_disp->samp_buffer != NULL )
    memset( sig_disp->samp_buffer, '\0', sig_disp->samp_width+1 ) ;

  /* Assegno il puntatore ai dati del nodo corrente
        Se non ho un nodo corrente utilizzo i puntatori precedenti
        sig_disp->curr_node == NULL: quando sono all' ultimo step della generazione della forma d'onda dei segnali posso avere segnali 
        che non hanno un nodo corrente con dati da visualizzare ma che devono essere rappresentati con displmay_same
  */    
  if ( sig_disp->curr_node != NULL ) {
    sam_cur = sig_disp->curr_node->data;
    // Controllo se esiste un nodo precedente
    if ( sig_disp->curr_node->prev != NULL )
      // Dati del nodo precedente, per effettuare confronti
      sam_pre = sig_disp->curr_node->prev->data;
  } else {
    sam_cur = NULL;
    sam_pre = sig_disp->last_node->data;
  }

  // Generazione stringa
  if ( p_mode == display_skip ) {
    if ( sig_disp->skip != NULL )
      sig_disp->skip( NULL, p_swidth, sig_disp->samp_buffer );
  }
  if ( p_mode == display_same ) {
    if ( sig_disp->same != NULL )
      sig_disp->same( sam_pre, p_swidth, sig_disp->samp_buffer );
  }
  if ( p_mode == display_data ) {
    if ( sam_pre != NULL && sam_cur != NULL && sam_pre->sample_value != sam_cur->sample_value ) {
      if ( sig_disp->change != NULL )
        sig_disp->change( sam_cur, p_swidth, sig_disp->samp_buffer );
    } else {
      if ( sig_disp->data != NULL )
        sig_disp->data( sam_cur, p_swidth, sig_disp->samp_buffer );
    }
    // Memorizzo l'ultimo nodo rappresentato
    sig_disp->last_node = sig_disp->curr_node;
    // Passo al nodo successivo
    sig_disp->curr_node = sig_disp->curr_node->next;
  }
  // Copio la strings generata per il sample nel buffer del segnale
  strcat( sig_disp->wave_buffer, sig_disp->samp_buffer );
  // Conversione implicita t_samp_width t_wave_width
  sig_disp->wave_length += p_swidth;
}

/**
 *  @brief      Estrazione stringa da visualizzare
 *  @details    Estrae il puntatore alla striniga generata e la sua lunghezza dalla struttura dati ->display
 *  @param      *s      Puntatore al segnale
 *  @param      **wave  Indirizzo del puntatore *wave
 *  @param      *width  Indirizzo del parametro width
 *  @return     *wave   Puntatore alla stringa generata
 *  @return     width   Lunghezza della stringa *wave
 *  @code
 *  @endcode
 */
void signal_display_get( t_signal *s, char **wave, t_wave_width *width ) {
  t_signal_data     *sig_data   = NULL;
  t_signal_display  *sig_disp   = NULL;

  if ( s != NULL && wave != NULL && width != NULL && s->data != NULL ) {
    sig_data= s->data;
    sig_disp= sig_data->display;

    *wave   = sig_disp->wave_buffer;
    *width  = sig_disp->wave_length;
  }
}

//  --------------------------------------------------------------------------------
//  Metodi di gestione di liste dei segnali e sincronizzazione dei segnali

/**
 *  @brief      Creazione lista segnali
 *  @details    Crea una lista di segnali vuota wires e ne inizializza i dati.
 *  @param      **w     Indirizzo del puntatore alla lista
 *  @return     *w      Puntatore a t_wires
 *  @code
 *  t_wires *w;
 *  wires_create( &w );
 *  @endcode
 */
void wires_create( t_wires **w ) {
  t_wires_data  *wd = NULL;
  list_create( w );
  if ( *w != NULL ) {
    wd = ( t_wires_data* ) calloc( 1, sizeof( t_wires_data ) );
    if ( wd != NULL ) {
      wd->cur_sample_id       = 0;
      wd->max_samples_signal   = MAX_SAMPLES_SIGNAL;
      (( t_wires* )(*w))->data = wd;
    }
  }
}

/**
 *  @brief      Aggiunta di un segnale alla lista wires
 *  @details    Crea un nodo t_wires_node e gli assegna la lista t_signal.
 *  @param      *w      Puntatore alla lista t_wires
 *  @param      *s      Puntatore alla lista t_signal
 *  @code
 *  t_wires  *w;
 *  t_signal *s;
 *  ... creazione wires, signali ecc..
 *  wires_add_signal( w, s );
 *  @endcode
 */
void wires_add_signal( t_wires *w, t_signal *s ) {
  t_wires_node     *n = NULL;
  t_signal_data   *sd = NULL;

  if ( w != NULL && s != NULL ) {
    node_create( &n );
    n->data = s;
    list_insert( w, n, -1 );
    //  Inserisco riferimento a wires
    sd        = s->data;
    sd->wires = w;
  }
}

/**
 *  @brief      Ricerca di un signal nella lista wires
 *  @details    Esegue la scansione della lista wires alla ricerca del nome del segnale
 *  @param      *w              Puntatore alla lista t_wires
 *  @param      *name           Nome del segnale da cercare
 *  @param      **ret_signal    Indirizzo del puntatore al segnale trovato oppure NULL
 *  @return     **ret_signal    Puntatore al segnale trovato oppure NULL
 *  @code
 *  t_wires  *w;
 *  t_signal *s0, *s1, ... *r;
 *  ... creazione wires, signali ecc..
 *  wires_get_signal( w, "Vcc", &r );
 *  @endcode
 */
void wires_get_signal( t_wires *w, char *name, t_signal **ret_signal ) {
  t_wires_node*   sn = NULL;

  if ( w != NULL && name != NULL ) {
    for ( sn = w->first; sn != NULL && strcmp( ((t_signal_data*)((t_signal*)sn->data)->data)->name, name ) != 0; sn = sn->next )
      ;
    if ( sn != NULL ) {
      *ret_signal = sn->data;
    } else {
      *ret_signal = NULL;
    }
  }
}

/**
 *  @brief      Aggiunta di un sample ad un signal
 *  @details    Aggiunge un sample, se il numero di sample nel segnale supera il limite impostato in max_sample_signal rimuove il primo sample e sincronizza tutti gli altri segnali eliminando i sample con id minore di quello eliminato. 
                Non ha senso rappresentare i segnali con campioni antecedenti ad uno eliminato.
 *  @param      *s          Puntatore al segnale
 *  @param      *s_val      Puntatore al valore da inserire nel sample. Può essere una struttura dati, una costante ecc...
 *  @param      inc_sample_id   Se true incrementa il contatore cur_sample_id. Va tenuto a false per aggiungere samples isocroni.
 *  @code
 *  t_wires  *w;
 *  t_signal *s;
 *  int       value = 5;
 *  ... creazione wires, signali ecc..
 *  wires_add_signal_sample( s, &value, true );
 *  @endcode
 */
void wires_add_signal_sample( t_signal *s, void *s_val, bool inc_sample_id ) {
  t_sample_id          i_del    = 0;
  void*                v_del    = NULL;
  t_wires                 *w    = NULL;
  t_wires_node     *wir_node    = NULL;
  t_wires_data       *w_data    = NULL;
  t_signal_node    *sig_node    = NULL;
  t_signal              *sig    = NULL;
  t_signal_data      *s_data    = NULL;
  t_sample              *sam    = NULL;

  if ( s == NULL )
    return;

  if ( s->data == NULL )
    return;
  s_data = s->data;

  if ( s_data->wires == NULL )
    return;
  w = s_data->wires;

  if ( w != NULL && w->data != NULL ) {
    w_data = w->data;
    if ( inc_sample_id )
      w_data->cur_sample_id++;
    if ( s->nodes >= w_data->max_samples_signal && w_data->max_samples_signal != 0 ) {
      // s->first ( t_signal_node* )
      if ( s->first != NULL ) {
        // s->first->data ( t_sample* )
        if ( s->first->data != NULL ) {
          //    Salvo l'id del sample da eliminare
          i_del  = ( ( t_sample* )s->first->data )->sample_id;
          /*    Una volta trovato il sample_id da eliminare, sincronizzo tutti i segnali 
                della board eliminando dagli altri segnali tutti i sample_id 
                minori o uguali a questo, tranne se il segnale è formato da un solo nodo.
          */
          //    Scansione di tutti i segnali nella board
          for ( wir_node = w->first; wir_node != NULL; wir_node = wir_node->next ) {
            /*  Scansiono tutti i sample del segnale
                *   Inizializzo ciclo con il primo nodo dei samples
                *   Esco se il nodo corrente è NULL o se il sample_id é maggiore di quello memorizzato
                      o se nel segnale è presente un solo sample
                *   Aggiorno il nuovo nodo sample con il nuovo primo nodo del segnale, dopo aver rimosso 
                      quello precedente ed aver aggiornato il puntatore ->first
            */
            for ( \
                  sig_node = ( ( t_signal* )wir_node->data )->first; \
                  ( ( t_signal* )wir_node->data )->nodes > 1 && sig_node != NULL && (( t_sample* )sig_node->data)->sample_id <= i_del; \
                  sig_node = ( ( t_signal* )wir_node->data )->first \
            ) {
              //    Elimino i dati del sample
              sample_destroy( ( ( t_sample** )&sig_node->data ), &v_del );
              //    Elimino il nodo del sample
              list_node_destroy( wir_node->data, &sig_node );
            }
          }
          //  Scansiono tutti i segnali per cercare il min_sample_id dopo la cancellazione dei samples
          i_del = w_data->cur_sample_id;
          for ( wir_node = w->first; wir_node != NULL; wir_node = wir_node->next ) {
            sig = wir_node->data;
            // Skip segnali senza nodi
            if ( sig->first != NULL ) {
              sam = sig->first->data;
              if ( sam->sample_id < i_del )
                i_del = sam->sample_id;
            }
          }
          w_data->min_sample_id = i_del;
        }
      }
    }
    signal_add_sample( s, w_data->cur_sample_id, s_val, NULL );
  }
}

/**
 *  @brief      Generazione waves forms
 *  @details    Inizializza la struttura dati ->display con i puntatori dei segnali correnti.
                Esegue la scansione di tutti i segnali generando per ciascuno una stringa rappresentante i valori dei samples secondo le callback di ciascun segnale e mantenendo i sample sincronizzati tramite i relativi sample_id.
 *  @param      *w          Puntatore alla lista t_wires
 *  @param      p_wwidth    Dimensione del buffer per la stringa da generare di ogni segnale
 *  @code
 *  t_wires  *w;
 *  creazione wires ...
 *  creazione segnali, inserimento segnali in wires ecc...
 *  inserimento samples nei segnali
 *  signal_display_create ... Configurazione display con larghezza, call back ecc... 
 *  wires_display_sample( w, 255 );
 *  signal_display_get( ... );
 *  printf( "%s", ... ); 
 *  @endcode
 *  @todo       Testare modalità display_skip
 */
void wires_display_sample( t_wires *w, t_wave_width p_wwidth ) {
  t_wires_node          *wir_node   = NULL;
  t_wires_data          *wir_data   = NULL;
  t_signal                     *s   = NULL;
  t_signal_data         *sig_data   = NULL;
  t_signal_display      *sig_disp   = NULL;
  t_signal_node    *sig_node_curr   = NULL;
  t_signal_node    *sig_node_last   = NULL;
  t_sample                 *sampl   = NULL;
  t_sample_id         i_sample_id   = 0;
  t_samp_width          min_width   = 0;
  int                       sdone   = 0;
  int                       stodo   = 0;
  int                         err   = 0;
  
  // Inizializzo display
  for ( wir_node = w->first; wir_node != NULL; wir_node = wir_node->next ) {
    s = wir_node->data;
    signal_display_start( s, p_wwidth );
  }
  
  wir_data = w->data;
  sdone = -1;
  err   = 0;
  for ( i_sample_id = wir_data->min_sample_id; \
        i_sample_id <= wir_data->cur_sample_id; \
        i_sample_id++ \
  ) {
    /*  Conto quanti samples sono presenti con questo i_sample_id e ricerco dimensione minima con cui rappresentare questo livello di sample
        Solo i valori dei sample con sample_id == i_sample_id vengono visualizzati quindi hanno bisogno dello spazio necessario.
        Gli altri vengono rappresentati con caratteri di riempimento secongo le loro funzioni call back ma in questo caso bastano meno caratteri di quelli che sarebbero necessari in modalità display_data
        Es. segnali di clock, display data con 2 caratteri, segnale Addr, display_data con 8 caratteri ma per display_same ne bastano 2
    */
    stodo     = 0;
    min_width = 0;
    for ( wir_node = w->first; \
          wir_node != NULL; \
          wir_node = wir_node->next \
    ) {
      s = wir_node->data;
      sig_data = s->data;
      // Test esistenza struttura dati ->display
      if ( sig_data->display != NULL ) {
        sig_disp = sig_data->display;
        sig_node_curr  = sig_disp->curr_node;
        if ( sig_node_curr != NULL ) {
          sampl  = sig_node_curr->data;
          if ( sampl->sample_id == i_sample_id && min_width < sig_disp->samp_dim ) {
            stodo++;
            min_width = sig_disp->samp_dim;
          }
        }
      }
    }
    if ( stodo > 0 ) {
      // Scansione ed estrazione segnali
      for ( wir_node = w->first; \
            err == 0 && wir_node != NULL; \
            wir_node = wir_node->next \
      ) {
        s = wir_node->data;
        sig_data = s->data;
        // Test esistenza struttura dati ->display
        if ( sig_data->display != NULL ) {
          sig_disp = sig_data->display;
          sig_node_curr  = sig_disp->curr_node;
          sig_node_last  = sig_disp->last_node;
          if ( sig_node_curr != NULL ) {
            sdone++;
            sampl  = sig_node_curr->data;
            if ( sampl->sample_id  < i_sample_id ) {
              printf( "Errore!\n" );
              err = -1;
            }
            if ( sampl->sample_id  > i_sample_id ) {
              if ( sig_disp->last_node == NULL ) {
                signal_display_sample( s, min_width, display_skip );
              } else {
                signal_display_sample( s, min_width, display_same );
              }
            }
            if ( sampl->sample_id == i_sample_id ) {
              signal_display_sample( s, min_width, display_data );
            }
          }
          /* Nella rappresentazione degli ultimi samples, devo portare in pari i segnali che hanno samples presecenti all' ultimo.
              S0: --.--.--.--.
              S1: **.**.**.
              S2: ++.++.
                Nell'ultimo step:
                S0 ha un sample da rappresentare che ha fatto incrementare stodo nel ciclo precedemte ed ha valorizzato min_width
                S1 ed S2 hanno sig_node_curr ( sig_disp->curr_node ) == NULL ma sig_disp->last_node punta all' ultimo nodo e possono essere
                  visualizzati con display_same
          */
          if ( sig_node_curr == NULL && stodo > 0 ) {
            if ( sig_node_last != NULL ) {
              signal_display_sample( s, min_width, display_same );
            } else {
              signal_display_sample( s, min_width, display_skip );
            }
          }
        }
      }
    }
  }
}
