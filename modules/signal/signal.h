#ifndef H_SIGNAL

#define H_SIGNAL

/**
 * @file     signal.h
 * @author   Stefano Busnelli    busnelli.stefano@gmail.com
 * @version  1.3.0
 * @brief    Segnali
 * @details  Strutture dati necessarie ad implementare una lista di segnali sincronizzati.
 * Una lista di segnali è una lista di tipo t_wires ( struct t_list ) contenente i segnali.
 *   ->data  è un puntatore ad una struct t_wires_data contenente informazioni comuni a tutti i segnali.
 *   ->first, ->last puntano al primo e all'ultimo nodo della lista t_wires_node ( struct t_node )
 *   Ogni t_wires_node->data punta ad una lista t_signal
 * Un segnale è una lista di tipo t_signal ( struct t_list ) contenente i campionamenti del segnale.
 *   ->data  conterrà il nome del segnale ( char* )
 *   ->first, ->last puntano al primo e all'ultimo nodo della lista t_signal_node ( t_node )
 *   Ogni t_signal_node->data punta ad un struct t_sample
 * Un sample è una struct t_sample
 *
 * t_wires                   ( t_list )
 *   data                    ( t_wires_data* )
 *     t_wires_data
 *   first, last             ( t_wires_node* )
 *     t_wires_node          ( t_node )
 *       data                ( t_signal* )
 *       prev, next          ( t_wires_node* )
 *         t_signal          ( t_list )
 *           data            ( char* )          : Nome del signal ( VCC, GND, CLOCK ... )
 *           first, last     ( t_signal_node* )
 *             t_signal_node ( t_node )
 *               data        ( t_sample* )      : Dati del sample
 *               next, prev  ( t_signal_node* ) : nodo successivo | precedente della lista t_signal
 *                 t_sample
 */

#include <stdlib.h>
#include <string.h>
#include "list.h"

#define MAX_SAMPLES_SIGNAL 0        ///<  Massimo numero di campioni da memorizzare in un segnale. Il numero minimo di samples memorizzabili in un signal è 2. Impostare 0 per nessun limite massimo

//  Dimensioni delle stringhe
typedef unsigned char       t_samp_width;
typedef unsigned short      t_wave_width;

//  Ridefinizioni di tipi
//      t_signal è una lista t_list in cui ogni t_signal_node->data punta ad un t_sample
typedef struct t_list t_signal;
typedef struct t_node t_signal_node;
//      t_wires è una lista t_list in cui ogni t_wires_node->data punta ad un t_signal
typedef struct t_list t_wires;
typedef struct t_node t_wires_node;

/** @brief      t_sample
 *  #details    t_sample è una struttura dati per memorizzare i dati di una campione di segnale
 */ 
typedef unsigned int t_sample_id;
typedef struct t_sample {
  t_sample_id   sample_id;          ///<  Progressivo temporale
  void*         sample_value;       ///<  Valore del segnale
} t_sample;


/** @brief      Rappresentazione del segnale
    @details    Prototipo di funzione per la rappresentazione visiva dei valori memorizzati nei campioni
    @param      t_sample        Puntatore al valore da estrarre
    @param      unsigned char   Numero di caratteri da generare
    @param      char*           Puntatore al buffer del campione ->samp_buffer
    @return     char*           Stringa generata dalla funzione
 */
typedef void ( *f_display_sample ) ( t_sample*, t_samp_width, char* );

/** @brief      Dati relativi alla rappresentazione grafica di un campione del segnale
    @details    La composizione della forma d'onda di un segnale avviene per step successivi in concorrenza con la composizione di tutti gli altri segnali analizzando in sequenza tutti i sample_id.
                Viene memorizzato il valore minimo di caratteri necessari per rappresentare un campione ( samp_dim ) che viene considerato assieme a quello di tutti gli altri segnali per trovare la dimensione di una colonna in cui visualizzare i vari segnali. Il sample visualizzato sarà in generale più grande di questo valore per avere una rappresentazione sincronizzata di tutti i segnali.
                Per fare questo vengono memorizzati in questa struttura dati i puntatori ai nodi che contengono i dati dei sample ( last_node, curr_node ) per poter richiamare in successione la funzione signal_display_sample.
                Questa struttura dati contiene i puntatori ai buffer dove salvare la stringa che rappresenta il sample corrente e l'intera forma d'onda ( samp_buffer, wave_buffer ).
                Contiene i puntatori alle funzione call back definite nel modulo che implementa il segnale per la conversioni dei valori in stringhe da visualizzare ( data, change, skip, same ).
                t_signal->data->display
 */
typedef struct t_signal_display {
  t_samp_width      samp_dim;       ///<  Dimensione minima della larghezza in caratteri di un campione del segnale
  t_samp_width      samp_width;     ///<  Dimensione del buffer del sample da visualizzare
  char*             samp_buffer;    ///<  Buffer contenente la stringa del campione
  t_wave_width      wave_width;     ///<  Dimensione del buffer della forma d'onda da visualizzare
  t_wave_width      wave_length;    ///<  Dimensione del buffer attualmente riempita.
  char*             wave_buffer;    ///<  Buffer contenente la stringa della forma d'onda da visualizzare
  t_signal_node*    last_node;      ///<  Puntatore all' ultimo nodo.
  t_signal_node*    curr_node;      ///<  Puntatore al nodo corrente.
  f_display_sample  data;           ///<  Callback per visualizzare il valore di un sample   """___"""___
  f_display_sample  change;         ///<  Callback per visualizzare il cambiamento di valore """\__/""\__
  f_display_sample  skip;           ///<  Callback per visualizzare l' assenza del sample    !!!___"""___
  f_display_sample  same;           ///<  Callback per visualizzare il valore precedente     ===___"""___
} t_signal_display;

/** @brief      Dati del segnale
    @details    t_signal->data
 */
typedef struct t_signal_data {
  char                 *name;       ///<  Nome del segnale
  t_wires              *wires;      ///<  Riferimento alla lista padre wires per poter mantenere tutti i segnali sincronizzati.
  t_signal_display     *display;    ///<  Puntatore a t_signal_display, creo la struct solo se mi serve
} t_signal_data;

/** @brief      Enumeration con le modalità di visualizzazione dei sample
    @details    Utilizzato come parametro di signal_display_sample
 */
enum e_signal_display_mode {
  display_data,                     ///<  Visualizza il valore successivo
  display_same,                     ///<  Continua con il valore precedente
  display_skip                      ///<  Inserisci spazio vuoto
};

typedef struct t_wires_data {
  t_sample_id   min_sample_id;      ///<  sample_id minore
  t_sample_id   cur_sample_id;      ///<  Sequence dei sample_id incrementata in wires_add_dignal_sample
  unsigned int  max_samples_signal; ///<  Numero massimo di samples da memorizzare per ogni segnale
} t_wires_data;

//  --------------------------------------------------------------------------------
//  Metodi di gestione dei samples

/** @brief      Creazione sample
 *  @details    Assegna memoria per la struct t_sample ed inizializza i valori
 *  @param      **sd    Indirizzo del puntatore a t_sample
 *  @param      s_id    Sample id
 *  @param      *s_val  Puntatore al valore da inserire nel sample
 *  @returns    *sd     Puntatore a t_sample
 */
void sample_create( t_sample **sd, t_sample_id s_id, void *s_val );

/** @brief      Distruzione sample
 *  @details    Libera la memoria utilizzata per la struct t_sample e restituisce il puntatore al valore inserito nel sample per una aventuale freei. Assegna a NULL il puntatore al t_sample.
 *  @param      **sd    Indirizzo del puntatore a t_sample
 *  @param      **sv    Indirizzo del puntatore al valore
 *  @returns    *sd     Puntatore a t_sample
 *  @returns    *sv     Puntatore al valore memorizzato nel sample
 */
void sample_destroy( t_sample **sd, void **sv );

/** @brief      Assegnamento valori sample
 *  @details    Assegna i valori ad un sample
 *  @param      *sd     Puntatore a t_sample
 *  @param      s_id    sample id
 *  @param      *s_val  Puntatore al valore da inserire nel sample
 */
void sample_set( t_sample *sd, t_sample_id s_id, void *s_val );

/** @brief      Inizializzazione valori sample
 *  @details    Azzera sample_id ed elimina sample_value liberando la memoria.
 *  @param      *sd Puntatore a t_sample
 */
void sample_clear( t_sample *sd );

//  --------------------------------------------------------------------------------
//  Metodi di gestione dei segnali

/**
 *  @brief      Creazione del segnale vuoto
 *  @details    Riserva la memoria per un segnale ed inizializza tutti i valori assegnando il nome
 *  @param      **s             Indirizzo del untatore al segnale t_signal
 *  @param      *signal_name    Puntatore a signal_name ( char* )
 *  @return     *s              Puntatore a t_signal
 *  @code
 *  t_signal *s;
 *  signal_create( &s, "Vcc" );
 *  @endcode
 */
void signal_create( t_signal **s, char *signal_name );

/**
 *  @brief      Lettura nome del segnale
 *  @details    Restituisce la stringa contenente il nome del segnale
 *  @param      *s              Puntatore al segnale t_signal
 *  @param      **signal_name   Indirizzo del puntatore a signal_name ( char** )
 *  @return     *signal_name    Puntatore a char *
 *  @code
 *  t_signal *s;
 *  char     *n;
 *  signal_create( &s, "Vcc" );
 *  signal_get_name( s, &n );
 *  printf( "%s", n );
 *  @endcode
 */
void signal_get_name( t_signal *s, char **signal_name );

/**
 *  @brief      Aggiunta di un sample al segnale
 *  @details    Crea un sample con i valori, lo aggiunge al segnale e ne restituisce l'indirizzo.
 *  @param      *s              Puntatore al segnale t_signal
 *  @param      s_id            Sample id ( Vedi sample_create )
 *  @param      *s_val          Indirizzo del valore da inserire ( Vedi sample_create )
 *  @param      **ret_sd        Indirizzo del puntatore a t_sample
 *  @return     *ret_sd         Puntatore a t_sample
 *  @code
 *  @endcode
 */
void signal_add_sample( t_signal *s, t_sample_id s_id, void *s_val, t_sample **ret_sd );

/**
 *  @brief      Creazione struttura dati display sample
 *  @details    Riserva la memoria per la struttura dati da assegnare a t_signal_data->display e ne inizializza i membri
 *  @param      **sd        Indirizzo del puntatore alla struct da creare
 *  @param      p_swidth    t_signal_display->samp_width
 *  @param      p_data      Puntatore a callback per t_signal_display->data     ( Anche NULL ) 
 *  @param      p_change    Puntatore a callback per t_signal_display->change   ( Anche NULL ) 
 *  @param      p_skip      Puntatore a callback per t_signal_display->skip     ( Anche NULL )
 *  @param      p_same      Puntatore a callback per t_signal_display->same     ( Anche NULL )
 *  @return     *sd         Puntatore a t_signal_display
 *  @code
 *  t_signal *s;
 *  signal_create( &s, "Vcc" );
 *  signal_display_create( &( ( t_signal_display* ) s->data )->display, 1, NULL, NULL, NULL );
 *  @endcode
 */
void signal_display_create( t_signal_display **sd, t_samp_width p_swidth, f_display_sample p_data, f_display_sample p_change, f_display_sample p_skip, f_display_sample p_same );

/**
 *  @brief      Inizio composizione forma d'onda del segnale
 *  @details    Riserva lo spazio per il buffer ->wave_buffer ed inizializza tutti i valori
 *              Inizializza o ri-inizializza i valori ogni volta che occorre rigenerare la forma d'onda del segnale
 *  @param      *s          Indirizzo del puntatore alla struct da creare
 *  @param      p_wwidth    Dimensioni stringa t_signal_display->wave_width
 *  @code
 *  t_signal *s;
 *  signal_create( &s, "Vcc" );
 *  signal_display_create( &( ( t_signal_display* ) s->data )->display, 1, NULL, NULL, NULL );
 *  @endcode
 */
void signal_display_start( t_signal *s, t_wave_width p_wwidth );

/**
 *  @brief      Generazione della forma d'onda del segnale
 *  @details    Produce la forma d'onda per il campione corrente ed accoda il risultato alla forma d'onda del segnale.
                Memorizza tutte le informazioni sullo stato nella struttura dati signal_display in modo tale che la funzione possa essere richiamata per passi successivi
 *  @param      *s          Puntatore al segnale
 *  @param      p_swidth    Dimensione in caratteri della rappresentazione del sample da passare alle call back
 *  @param      p_mode      display_data        Converte in lavore del sample in in stringa e passa al nodo successivo
 *                          display_skip        Inserisce spazio vuoto
 *                          display_same        Continua a visualizzare il valore dell' ultimo sample
 *  @code
 *  t_signal *s;
 *  signal_create( &s, "Vcc" );
 *  signal_display_create( &( ( t_signal_display* ) s->data )->display, 1, NULL, NULL, NULL );
 *  @endcode
 *  @todo       Testare modalità display_skip
 */
void signal_display_sample( t_signal *s, t_samp_width p_swidth, enum e_signal_display_mode p_mode );

/**
 *  @brief      Estrazione stringa da visualizzare
 *  @details    Estrae il puntatore alla striniga generata e la sua lunghezza dalla struttura dati ->display
 *  @param      *s      Puntatore al segnale
 *  @param      **wave  Indirizzo del puntatore *wave
 *  @param      *width  Indirizzo del parametro width
 *  @return     *wave   Puntatore alla stringa generata
 *  @return     width   Lunghezza della stringa *wave
 *  @code
 *  @endcode
 */
void signal_display_get( t_signal *s, char **wave, t_wave_width *width );

//  --------------------------------------------------------------------------------
//  Metodi di gestione di liste dei segnali e sincronizzazione dei segnali

/**
 *  @brief      Creazione lista segnali
 *  @details    Crea una lista di segnali vuota wires e ne inizializza i dati.
 *  @param      **w     Indirizzo del puntatore alla lista
 *  @return     *w      Puntatore a t_wires
 *  @code
 *  t_wires *w;
 *  wires_create( &w );
 *  @endcode
 */
void wires_create( t_wires **w );

/**
 *  @brief      Aggiunta di un segnale alla lista wires
 *  @details    Crea un nodo t_wires_node e gli assegna la lista t_signal.
 *  @param      *w      Puntatore alla lista t_wires
 *  @param      *s      Puntatore alla lista t_signal
 *  @code
 *  t_wires  *w;
 *  t_signal *s;
 *  ... creazione wires, signali ecc..
 *  wires_add_signal( w, s );
 *  @endcode
 */
void wires_add_signal( t_wires *w, t_signal *s );

/**
 *  @brief      Ricerca di un signal nella lista wires
 *  @details    Esegue la scansione della lista wires alla ricerca del nome del segnale
 *  @param      *w              Puntatore alla lista t_wires
 *  @param      *name           Nome del segnale da cercare
 *  @param      **ret_signal    Indirizzo del puntatore al segnale trovato oppure NULL
 *  @return     **ret_signal    Puntatore al segnale trovato oppure NULL
 *  @code
 *  t_wires  *w;
 *  t_signal *s0, *s1, ... *r;
 *  ... creazione wires, signali ecc..
 *  wires_get_signal( w, "Vcc", &r );
 *  @endcode
 */
void wires_get_signal( t_wires *w, char *name, t_signal **ret_signal );

/**
 *  @brief      Aggiunta di un sample ad un signal
 *  @details    Aggiunge un sample, se il numero di sample nel segnale supera il limite impostato in max_sample_signal rimuove il primo sample e sincronizza tutti gli ltri segnali eliminando i sample con id minore di quello eliminato. Non ha senso rappresentare i segnali con campioni antecedenti ad uno eliminato.
 *  @param      *s          Puntatore al segnale
 *  @param      *s_val      Puntatore al valore da inserire nel sample. Può essere una struttura dati, una costante ecc...
 *  @param      inc_sample_id   Se true incrementa il contatore cur_sample_id. Va tenuto a false per aggiungere samples isocroni.
 *  @code
 *  t_wires  *w;
 *  t_signal *s;
 *  int       value = 5;
 *  ... creazione wires, signali ecc..
 *  wires_add_signal_sample( w, s, &value, true );
 *  @endcode
 */
void wires_add_signal_sample( t_signal *s, void *s_val, bool inc_sample_id );

/**
 *  @brief      Generazione waves forms
 *  @details    Inizializza la struttura dati ->display con i puntatori dei segnali correnti.
                Esegue la scansione di tutti i segnali generando per ciascuno una stringa rappresentante i valori dei samples secondo le callback di ciascun segnale e mantenendo i sample sincronizzati tramite i relativi sample_id.
 *  @param      *w          Puntatore alla lista t_wires
 *  @param      p_wwidth    Dimensione del buffer per la stringa da generare di ogni segnale
 *  @code
 *  t_wires  *w;
 *  creazione wires ...
 *  creazione segnali, inserimento segnali in wires ecc...
 *  inserimento samples nei segnali
 *  signal_display_create ... Configurazione display con larghezza, call back ecc... 
 *  wires_display_sample( w, 255 );
 *  signal_display_get( ... );
 *  printf( "%s", ... ); 
 *  @endcode
 *  @todo       Testare modalità display_skip
 */
void wires_display_sample( t_wires* w, t_wave_width p_wwidth );

#endif
