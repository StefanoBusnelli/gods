#ifndef H_CLOCK

#define H_CLOCK

/**
 * @file    clock.h
 * @author  Stefano Busnelli    busnelli.stefano@gmail.com
 * @version 1.3.0
 * @brief   Clock
 * @details Implementazione del device clock
 */

#include "device.h"
#include "time.h"

typedef unsigned int        u_int;
typedef unsigned long int   ul_int;
typedef const u_int         cu_int;

#ifndef _v_gnd
#define _v_gnd
extern cu_int val_gnd;
#endif

#ifndef _v_vcc
#define _v_vcc
extern cu_int val_vcc;
#endif

//  Dimensioni ed indici array params
#define CLOCK_P_NUM             3
#define CLOCK_P_US_ON           0
#define CLOCK_P_US_OFF          1
#define CLOCK_P_MAX_CLOCKS      2

//  Dimensioni ed indici array methods
#define CLOCK_METHODS_NUM           3
#define CLOCK_METHODS_SETTON        0
#define CLOCK_METHODS_SETTOFF       1
#define CLOCK_METHODS_SETMAXCLOCKS  2

//  Dimensioni ed indici array segnali gestiti dal device, puntatori ai segnali nella board
#define CLOCK_S_NUM             3
#define CLOCK_S_GND             0
#define CLOCK_S_VCC             1
#define CLOCK_S_CLK             2

//  Stato interno del clock
#define CLOCK_STATE_DOWN        0
#define CLOCK_STATE_UP          1

typedef struct t_clock_data t_clock_data;
typedef struct t_clock_data {
  char                  up_down;        //  Stato attuale del segnale Alto/Basso
  unsigned long int     num_clocks;     //  Numero di cicli di clock eseguiti
} tt_clock_data;

//  Tipi

typedef struct timespec t_timespec;

//  Metodi pubblici

/** @brief    Clock create
 *  @details  Crea il device, imposta il nome ed assegna le call back init, run e end
 *  @param    **d           Indirizzo del puntatore a t_device
 *  @param    *name         Nome del device: "CLOCK"
 *  @param    **sig_names   Nomi dei segnali: { "Gnd", "Vcc", "ClK" }, se NULL vengono inizializzati valori di default
 *  @param    sig_num       Numero di stringhe nell' array
 *  @returns  *d            Puntatore a t_device
 *  @code
 *  t_device  d_clk0  = NULL;
 *  t_device  d_clk1  = NULL;
 *  char      *sig[]  = { "Gnd", "Vcc", "ClK" };
 *  clock_create( &d_clk0, "Clock0", NULL, 0 );
 *  clock_create( &d_clk1, "Clock1", sig, 3 );
 *  @endcode
 */
void clock_create( t_device **d, char *name, char **sig_names );

#endif
