#include <stdlib.h>
#include <stdio.h>

#include "clock.h"
#include "board.h"

/*  Dump liste
 */

void dump_signal( t_signal* list ) {
  char* d = NULL;
  if( list != NULL ) {
    signal_get_name( list, &d );
    if ( d != NULL ) {
      printf( "Segnale: \033[33;1m%8s\033[0m\tSamples: %6d", d, list->nodes );
    }
  }
}

void dump_signal_node( t_signal_node* node ) {
  t_sample* s = NULL;
  if ( node != NULL ) {
    s = ( t_sample* )node->data;
    if ( s != NULL && s->sample_value != NULL ) {
      printf( "Sample ID: %6d Sample Value: ( %16p -> \033[36;1m%3d\033[0m )", ( int ) s->sample_id, s->sample_value, *( int* )s->sample_value );
    } else {
      printf( "Sample ID: %6d Sample Value: %16p", ( int ) s->sample_id, s->sample_value );
    }
  }
}

void dump_wires_node( t_wires_node* node ) {
  t_wires* s = NULL;
  if( node != NULL ) {
    s = ( t_wires* )node->data;
    if ( s != NULL && s->data != NULL ) {
      list_dump( s, dump_signal, dump_signal_node, "\n    ", NULL );
    }
  }
}

void dump_wires( t_wires* list ) {
  t_wires_data* d = NULL;
  if( list != NULL ) {
    d = ( t_wires_data* )list->data;
    if ( d != NULL ) {
      printf( "\033[31;1mWires:\033[0m\tSegnali: %3d\tCurr sample id: %6d\tMax Samples x signal: %6d", list->nodes, d->cur_sample_id, d->max_samples_signal );
    }
  }
}

/*  Display segnale
 */

void digi_data( t_sample *sam, t_samp_width p_width, char *ret ) {
  if( ret == NULL )
    return;

  if ( *( ( cu_int*)sam->sample_value ) == val_gnd )
    memset( ret, '_', p_width );
  if ( *( ( cu_int*)sam->sample_value ) == val_vcc )
    memset( ret, '^', p_width );

  *(ret + p_width)   = '\0';
}

void digi_change( t_sample *sam, t_samp_width p_width, char *ret ) {
  if( ret == NULL )
    return;

  *ret = '|';
  if ( *( ( cu_int*)sam->sample_value ) == val_gnd )
    memset( ret+1, '_', p_width-1 );
  if ( *( ( cu_int*)sam->sample_value ) == val_vcc )
    memset( ret+1, '^', p_width-1 );

  *(ret + p_width)   = '\0';
}

void digi_cont( t_sample *sam, t_samp_width p_width, char *ret ) {
  if( ret == NULL )
    return;

  if ( *( ( cu_int*)sam->sample_value ) == val_gnd )
    memset( ret, '_', p_width );
  if ( *( ( cu_int*)sam->sample_value ) == val_vcc )
    memset( ret, '^', p_width );
  
  *(ret + p_width)   = '\0';
}

void digi_skip( t_sample *sam, t_samp_width p_width, char *ret ) {
  if( ret == NULL )
    return;

  memset( ret, ' ', p_width );

  if ( *( ( cu_int*)sam->sample_value ) == val_gnd )
    *(ret + p_width-1) = '.';

  *(ret + p_width)   = '\0';
}


/*  Main
 */

int main( int argc, char **argv ) {
  t_board*                       b  = NULL;
  t_device*                      d  = NULL;
  char                *sig_names[]  = { "Gnd", "Vcc", "Clk" };
  ul_int            uli_max_clocks  = 5;

  //    Test lista board->devices 
//  t_device*         dv  = NULL;
//  t_devices_node*   n   = NULL;

  printf( "\n" );

  printf( "\n---\tCreo board vuota" );

  board_create( &b );

  printf( "\nBoard (t_board):                         %16p %16ld bytes", b, sizeof( b ) );

  if ( b == NULL ) { 
    printf( "\n---\tImpossibile creare la board\n" );
    return 1;
  }

  printf( " %16ld bytes", sizeof( *b ) );

  // Imposto max_samples_signal
  ( ( t_wires_data* )b->wires->data )->max_samples_signal = 8;

  printf( "\n---\tSegnali presenti:" );
  list_dump( b->wires, dump_wires, dump_wires_node, "\n  ", NULL );

  // --------

  printf( "\n---\tCreo device CLOCK" );

  //clock_create( &d, "Clock", NULL );
  clock_create( &d, "Clock", sig_names );

  printf( "\nDevice (t_device):                       %16p %16ld bytes", d, sizeof( d ) );

  if ( d == NULL ) { 
    printf( "\n---\tImpossibile creare il device\n" );
    return 1;
  }

  printf( " %16ld bytes", sizeof( *d ) );

  // --------

  printf( "\n---\tPlug in CLOCK" );
  board_plug_device( b, d );

  printf( "\n---\tSegnali presenti:" );
  list_dump( b->wires, dump_wires, dump_wires_node, "\n  ", NULL );

  // Dopo aver montato il device, imposto max clocks
  d->methods[ CLOCK_METHODS_SETMAXCLOCKS ].func( d, &uli_max_clocks );

  // Dopo aver montato il device e creato i segnali creo la struttura dati ->display
  t_wires_node      *wn = NULL;
  t_signal           *s = NULL;
  for ( wn = b->wires->first; wn != NULL; wn = wn->next ) {
    s   = wn->data;
    signal_display_create( \
      &( ( t_signal_data* ) s->data )->display, \
      2, \
      digi_data, \
      digi_change, \
      digi_skip, \
      digi_cont \
    );
  }

  // --------

  printf( "\n---\tDevice Run:" );
  //    Imposto lo stato run ( di default è pause )
  board_run_state( b, rm_run );

  // Avvio tutti i devices registrati in board->devices ( 1 in questo caso )
//  dv = NULL;
//  for ( n=b->devices->first; n != NULL; n=n->next ) {
//    dv = ( t_device* )n->data;
//    dv->run( dv );
//  }
  d->run( d );

  printf( "\n---\tSegnali presenti:" );
  list_dump( b->wires, dump_wires, dump_wires_node, "\n  ", NULL );

  printf( "\n---\tDevice End:" );
  // Fermo tutti i devices registrati in board->devices ( 1 in questo caso )
//  dv = NULL;
//  for ( n=b->devices->first; n != NULL; n=n->next ) {
//    dv = ( t_device* )n->data;
//    dv->end( dv );
//  }
  d->end( d );

  printf( "\n---\tSegnali presenti:" );
  list_dump( b->wires, dump_wires, dump_wires_node, "\n  ", NULL );
  
  // Genero waves
  printf( "\n---\tGenero waves:\n" );
  wires_display_sample( b->wires, 127 );

  // Visualizzo le waves
  printf( "\n---\tWaves:\n" );
  t_signal_data    *sdt = NULL;
  char            *wave = NULL;
  t_wave_width    width = 0;
  for ( wn = b->wires->first; wn != NULL; wn = wn->next ) {
    s = wn->data;
    sdt = s->data;
    signal_display_get( s, &wave, &width );
    printf( "%8s %s\n", sdt->name, wave );
  }

  printf( "\nOK.\n" );

  return 0;
}
