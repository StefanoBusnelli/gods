#include <stdlib.h>
#include <stdio.h>

#include "clock.h"
#include "board.h"

/*  Main
 */

int main( int argc, char **argv ) {
  t_board*                       b  = NULL;
  t_device*                      d  = NULL;
  char                *sig_names[]  = { "Gnd", "Vcc", "Clk" };
  ul_int            uli_max_clocks  = 5;

  board_create( &b );
  if ( b == NULL ) { 
    return 1;
  }

  // --------

  clock_create( &d, "Clock", sig_names );
  if ( d == NULL ) { 
    return 1;
  }

  // --------

  board_plug_device( b, d );

  // Dopo aver montato il device, imposto max clocks
  d->methods[ CLOCK_METHODS_SETMAXCLOCKS ].func( d, &uli_max_clocks );

  // --------

  //    Imposto lo stato run ( di default è pause )
  board_run_state( b, rm_pause );
  d->run( d );
  d->end( d );

  return 0;
}
