/**
 * @file    clock.c
 * @author  Stefano Busnelli    busnelli.stefano@gmail.com
 * @version 1.3.0
 * @brief   Clock
 * @details Implementazione del device clock
 */

#include "clock.h"

//  Costanti

cu_int val_gnd = 0;
cu_int val_vcc = 5;

//  -----------------------------------------------------------------------------------------------------------------
//  Metodi d->methods

/** @brief      Metodo Set TOn
 *  @details     Funzione di tipo t_device_methods, da assegnare agli elementi dell'array t_device->methods
 *  @param      *d  Puntatore a t_device, per accedere ai valori del device
 *  @param      *t  Puntatore al valore da inserire nel parametro d->params[ CLOCK_P_US_ON ] di tipo t_timespec*
 */
void clock_set_t_on( t_device *d, void *t ) {
  *( t_timespec* ) d->params[ CLOCK_P_US_ON  ].value = *(( t_timespec* )t);
}

/** @brief      Metodo Set TOff
 *  @details    Funzione di tipo t_device_methods, da assegnare agli elementi dell'array t_device->methods
 *  @param      *d  Puntatore a t_device, per accedere ai valori del device
 *  @param      *t  Puntatore al valore da inserire nel parametro d->params[ CLOCK_P_US_OFF ] di tipo t_timespec*
 */
void clock_set_t_off( t_device *d, void *t ) {
  *( t_timespec* ) d->params[ CLOCK_P_US_OFF ].value = *(( t_timespec* )t);
}

/** @brief      Metodo Set Num clocks
 *  @details    Funzione di tipo t_device_methods, da assegnare agli elementi dell'array t_device->methods
 *  @param      *d  Puntatore a t_device, per accedere ai valori del device
 *  @param      *t  Puntatore al valore da inserire nel parametro d->params[ CLOCK_P_MAX_CLOCKS ] di tipo ul_int*
 */
void clock_set_max_clocks( t_device *d, void *t ) {
  *( ul_int* ) d->params[ CLOCK_P_MAX_CLOCKS ].value = *(( ul_int* )t);
}

//  -----------------------------------------------------------------------------------------------------------------
//  Metodi d->init, d->run, d->end

/** @brief    Init call back
 *  @details  Callback richiamata da t_device->init ed assegnata a t_device->internal->cb_init
 *            Crea ed inizializza:
 *            t_device->data      puntatore a struct t_clock_data, inizializzazione dei membri della struct
 *            t_device->params    array di puntatori ad elementi t_device_params, inizializzazione dei singoli elementi
 *            t_device->methods   array di puntatori a funzioni t_device_methods, inizializzazione dei singoli elementi
 *            t_device->signals   array di puntatori a t_signal, inizializzazione dei singoli elementi
 *  @param    *d                  Puntatore a t_device, per accedere ai valori del device
 */
void clock_init( t_device *d ) {
  //    Imposto lo stato interno
  d->data = ( t_clock_data* )calloc( 1, sizeof( t_clock_data ) );
  (( t_clock_data* )d->data)->up_down      = CLOCK_STATE_DOWN;
  (( t_clock_data* )d->data)->num_clocks   = 0;

  //    Imposto i parametri
  d->params  = ( t_device_param* )calloc( CLOCK_P_NUM, sizeof( t_device_param ) );
  d->params[ CLOCK_P_US_ON  ].name  = "T On (ns)";
  d->params[ CLOCK_P_US_ON  ].value = ( t_timespec* )calloc( 1, sizeof( t_timespec ) );
  *( t_timespec* ) d->params[ CLOCK_P_US_ON  ].value = ( t_timespec ){ 0, 100000000 };

  d->params[ CLOCK_P_US_OFF ].name  = "T Off (ns)";
  d->params[ CLOCK_P_US_OFF ].value = ( t_timespec* )calloc( 1, sizeof( t_timespec ) );
  *( t_timespec* ) d->params[ CLOCK_P_US_OFF ].value = ( t_timespec ){ 0, 100000000 };

  d->params[ CLOCK_P_MAX_CLOCKS ].name  = "Max Clocks";
  d->params[ CLOCK_P_MAX_CLOCKS ].value = ( ul_int* )calloc( 1, sizeof( ul_int ) );
  *( ul_int* )d->params[ CLOCK_P_MAX_CLOCKS ].value = 0;

  //    Imposto i metodi
  d->methods = ( t_device_methods* )calloc( CLOCK_METHODS_NUM, sizeof( t_device_methods ) );
  d->methods[ CLOCK_METHODS_SETTON  ].name = "Set T On";
  d->methods[ CLOCK_METHODS_SETTON  ].func = ( f_device_par )clock_set_t_on;
  d->methods[ CLOCK_METHODS_SETTOFF ].name = "Set T Off";
  d->methods[ CLOCK_METHODS_SETTOFF ].func = ( f_device_par )clock_set_t_off;
  d->methods[ CLOCK_METHODS_SETMAXCLOCKS ].name = "Set Max Clocks";
  d->methods[ CLOCK_METHODS_SETMAXCLOCKS ].func = ( f_device_par )clock_set_max_clocks;
}

/** @brief    Run call back
 *  @details  Callback richiamata da t_device->run ed assegnata a t_device->internal->cb_run
 *            Qui viene implementato il funzionamento del device
 *  @param    *d Puntatore a t_device, per accedere ai valori del device
 */
void clock_run( t_device *d ) {
  t_clock_data     *c_data = d->data;

  //    Funzionamento del Clock
  switch ( c_data->up_down ) {
    case CLOCK_STATE_DOWN:
      c_data->up_down         = CLOCK_STATE_UP;
      wires_add_signal_sample( d->signals[ CLOCK_S_CLK ], ( u_int* )&val_vcc, true );
      nanosleep( ( t_timespec* ) d->params[ CLOCK_P_US_ON  ].value, NULL );
      break;
    case CLOCK_STATE_UP:
      c_data->up_down         = CLOCK_STATE_DOWN;
      wires_add_signal_sample( d->signals[ CLOCK_S_CLK ], ( u_int* )&val_gnd, true );
      nanosleep( ( t_timespec* ) d->params[ CLOCK_P_US_OFF ].value, NULL );
      c_data->num_clocks     += 1;
      break;
  }
  //    Condizioni di uscita dak main loop del device
  d->internal.main_loop &= ( *( ul_int* )d->params[ CLOCK_P_MAX_CLOCKS ].value == 0 || c_data->num_clocks < *( ul_int* )d->params[ CLOCK_P_MAX_CLOCKS ].value );
}

/** @brief    End call back
 *  @details  Callback richiamata da t_device->end ed assegnata a t_device->internal->cb_end
 *            Qui viene implemento il rilascio delle risorse al termine dell'esecuzione del device
 *  @param    *d Puntatore a t_device, per accedere ai valori del device
 */
void clock_end( t_device *d ) {
  wires_add_signal_sample( d->signals[ CLOCK_S_CLK ], ( u_int* )&val_gnd, true );
}

//  -----------------------------------------------------------------------------------------------------------------
//  Metodi pubblici

/** @brief    Clock create
 *  @details  Crea il device, imposta il nome ed assegna le call back init, run e end
 *  @param    **d           Indirizzo del puntatore a t_device
 *  @param    *name         Nome del device: "CLOCK"
 *  @param    **sig_names   Nomi dei segnali: { "Gnd", "Vcc", "ClK" }, se NULL vengono inizializzati valori di default
 *  @returns  *d            Puntatore a t_device
 *  @code
 *  t_device  d_clk0  = NULL;
 *  t_device  d_clk1  = NULL;
 *  char      *sig[]  = { "Gnd", "Vcc", "ClK" };
 *  clock_create( &d_clk0, "Clock0", NULL );
 *  clock_create( &d_clk1, "Clock1", sig );
 *  @endcode
 */
void clock_create( t_device **d, char *name, char **sig_names ) {
  if ( sig_names == NULL ) {
    sig_names = ( char** )calloc( CLOCK_S_NUM, sizeof( char* ) );
    sig_names[ CLOCK_S_GND ] = "GND";
    sig_names[ CLOCK_S_VCC ] = "VCC";
    sig_names[ CLOCK_S_CLK ] = "CLK";
  }
  device_create( d, name, sig_names, CLOCK_S_NUM, clock_init, clock_run, clock_end );
}
